using UnityEngine;
using System.Collections;

// function : by adjusting transform, position and scale, automatically scale the GUIElements,
// like GUITexture in transform child
// where the transform child set the render Rect by transform.position and scale



[AddComponentMenu("gui control/adjustGUIElementSize")]
public class adjustGUIElementSize : MonoBehaviour
{
	const int BASE_GUI_WIDTH = 960;
	const int BASE_GUI_HEIGHT = 640;
	
	// public TextAnchor	textAnchor	= TextAnchor.LowerCenter;
	int prevScreenWidth = 0;
	int prevScreenHeight = 0;

	// Use this for initialization
	void Start()
	{
		recalcScreenSize();
		
		if (Application.platform == RuntimePlatform.IPhonePlayer 
			|| Application.platform == RuntimePlatform.Android)
		{
			enabled = false; // screen won't change any more
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Screen.width != prevScreenWidth || Screen.height != prevScreenHeight)
		{
			recalcScreenSize();
		}
	}
	
	[ContextMenu("recalcScreenSize")]
	public void recalcScreenSize()
	{
		Vector3 newScale = transform.localScale;
		// Vector3 newTranslate = transform.localPosition;
		if (Screen.height * BASE_GUI_WIDTH >= Screen.width * BASE_GUI_HEIGHT)
		{
			// initial scale are 1,1
			//newScale.x = 1.0f;
			//newScale.y = (float)(Screen.width * BASE_GUI_HEIGHT) / (Screen.height * BASE_GUI_WIDTH);
			//newTranslate.x = 0.0f;
			//newTranslate.y = 0.0f; // for bottom align, top align use: 1.0f - newScale.y
			
			// initial scale are 1/BASE_GUI_WIDHT, 1/BASE_GUI_HEIGHT
			newScale.x = 1.0f / BASE_GUI_WIDTH;
			newScale.y = (float)Screen.width / BASE_GUI_WIDTH / Screen.height;
			
			// textAnchor is no more
			//newTranslate.x = 0.0f;
			//~ switch(textAnchor)
			//~ {
			//~ case TextAnchor.MiddleLeft:
			//~ case TextAnchor.MiddleCenter:
			//~ case TextAnchor.MiddleRight:
				//~ newTranslate.y = (1.0f - newScale.y * BASE_GUI_HEIGHT) * 0.5f;
				//~ break;
			//~ case TextAnchor.UpperLeft:
			//~ case TextAnchor.UpperCenter:
			//~ case TextAnchor.UpperRight:
				//~ newTranslate.y = 1.0f - newScale.y * BASE_GUI_HEIGHT;
				//~ break;
			//~ case TextAnchor.LowerLeft:
			//~ case TextAnchor.LowerCenter:
			//~ case TextAnchor.LowerRight:
			//~ default:
				//~ newTranslate.y = 0.0f;
				//~ break;
			//~ }
		}
		else
		{
			// initial scale are 1,1
			//newScale.x = (float)(Screen.height * BASE_GUI_WIDTH) / (Screen.width * BASE_GUI_HEIGHT);
			//newScale.y = 1.0f;
			//newTranslate.x = (1.0f - newScale.x) * 0.5f; // center
			//newTranslate.y = 0.0f;
			
			// initial scale are 1/BASE_GUI_WIDHT, 1/BASE_GUI_HEIGHT
			newScale.x = (float)Screen.height / BASE_GUI_HEIGHT / Screen.width;
			newScale.y = 1.0f / BASE_GUI_HEIGHT;
			
			// textAnchor is no more
			//~ switch(textAnchor)
			//~ {
			//~ case TextAnchor.UpperCenter:
			//~ case TextAnchor.MiddleCenter:
			//~ case TextAnchor.LowerCenter:
				//~ newTranslate.x = (1.0f - newScale.x * BASE_GUI_WIDTH) * 0.5f;
				//~ break;
			//~ case TextAnchor.UpperRight:
			//~ case TextAnchor.MiddleRight:
			//~ case TextAnchor.LowerRight:
				//~ newTranslate.x = 1.0f - newScale.x * BASE_GUI_WIDTH;
				//~ break;
			//~ case TextAnchor.UpperLeft:
			//~ case TextAnchor.MiddleLeft:
			//~ case TextAnchor.LowerLeft:
			//~ default:
				//~ newTranslate.x = 0.0f;
				//~ break;
			//~ }
			//~ newTranslate.y = 0.0f;
		}
		transform.localScale = newScale;
		// transform.localPosition = newTranslate;
		prevScreenWidth = Screen.width;
		prevScreenHeight = Screen.height;
	}
}
