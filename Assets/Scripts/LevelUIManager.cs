﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelUIManager : MonoBehaviour {

	public Image levelTitle;

	public Image level_1_title;
	public Image level_2_title;
	public Image level_3_title;
	public Image level_4_title;
	public Image level_5_title; 

	public GameObject mission4;
	public GameObject mission5;

	public GameObject chapter_panel;
	public GameObject level_panel;


	public GameObject After;
	public Text content;

	public Image recom_ten; 
	public Image recom_unit; 
	
	int[] recom_lv = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,25,30};
	Vector3 visible = new Vector3(1,1,1);
	Vector3 invisible = new Vector3(0,0,0);

	void Start(){

		//PlayerPrefs.SetString("Stage", "1-5");

		string stage = PlayerPrefs.GetString ("Stage");
		
		int c = int.Parse(stage.Substring (0, 1));
		int l = int.Parse(stage.Substring (2, 1));
		
		int _c = c;
		
		if(l == 5)
			_c++;


		GameObject chapterPanel = GameObject.Find ("Chapters");

		for(int i = _c+1; i < 6; i++)
		{

			GameObject m = chapterPanel.transform.Find("chapter" + i.ToString()).gameObject;
			Image image = m.transform.Find ("title").gameObject.GetComponent<Image>();
			image.color = new Color(1f,1f,1f,0.5f);
			Button btn = m.transform.Find ("overlay").gameObject.GetComponent<Button>();
			btn.interactable = false;
			
		}



	}

	public void showLevel(int level)
	{

		string stage = PlayerPrefs.GetString ("Stage");

		int c = int.Parse(stage.Substring (0, 1));
		int l = int.Parse(stage.Substring (2, 1));

		int _c = c;

		if(l == 5)
			_c++;

		if(level > _c){
			return;
		}

		GameObject chapter_go = GameObject.Find ("Chapter") as GameObject;
		GameObject level_go = GameObject.Find ("Level") as GameObject;
		
		chapter_go.transform.localScale = invisible;
		level_go.transform.localScale = visible;
			

		levelTitle.sprite = Resources.Load <Sprite>("LevelUI/NAME/"+level+"/T1");

		level_1_title.sprite = Resources.Load <Sprite>("LevelUI/NAME/"+level+"/1");
		level_2_title.sprite = Resources.Load <Sprite>("LevelUI/NAME/"+level+"/2");
		level_3_title.sprite = Resources.Load <Sprite>("LevelUI/NAME/"+level+"/3");
		if(level != 5)
		{
			mission4.SetActive(true);
			mission5.SetActive(true);

			level_4_title.sprite = Resources.Load <Sprite>("LevelUI/NAME/"+level+"/4");
			level_5_title.sprite = Resources.Load <Sprite>("LevelUI/NAME/"+level+"/5");

		}
		else
		{

			mission4.SetActive(false);
			mission5.SetActive(false);

		}

		Global.level = 1;

		int offset = (Global.chapter - 1) * 5 + 1;

		int lv = recom_lv [offset - 1];

		if(lv < 10)
		{
			recom_ten.sprite = Resources.Load <Sprite>("ChapterUI/建議等級/NUMBERS[size_175]/0");
			recom_unit.sprite = Resources.Load <Sprite>("ChapterUI/建議等級/NUMBERS[size_175]/" + lv);
		}
		else{
			recom_ten.sprite = Resources.Load <Sprite>("ChapterUI/建議等級/NUMBERS[size_175]/" + lv/10);
			recom_unit.sprite = Resources.Load <Sprite>("ChapterUI/建議等級/NUMBERS[size_175]/" + lv%10);
		}

		

		
		//level_1_title.color = new Color(1f,1f,1f,0.5f);
		
		int level_index = (c - 1) * 5 + l;

		GameObject levelPanel = GameObject.Find ("Levels");

		if(_c > Global.chapter){

			for(int i = 1; i < 6; i++)
			{
				GameObject m = levelPanel.transform.Find("mission" + i.ToString()).gameObject;
				Image image = m.transform.Find ("Image").gameObject.GetComponent<Image>();
				image.color = new Color(1f,1f,1f,1f);
				Button btn = m.transform.Find ("overlay").gameObject.GetComponent<Button>();
				btn.interactable = true;
				GameObject newSign = m.transform.Find ("new").gameObject;
				newSign.SetActive (false);
				
			}

		}

		for(int i = 1; i < 6; i++)
		{
			//mission1
			if(((Global.chapter - 1) * 5 + i - level_index == 1)){
				GameObject m = levelPanel.transform.Find("mission" + i.ToString()).gameObject;
				GameObject newSign = m.transform.Find ("new").gameObject;
				newSign.SetActive (true);
			}
			else if(((Global.chapter - 1) * 5 + i - level_index > 1)){
				GameObject m = levelPanel.transform.Find("mission" + i.ToString()).gameObject;
				Image image = m.transform.Find ("Image").gameObject.GetComponent<Image>();
				image.color = new Color(1f,1f,1f,0.5f);
				Button btn = m.transform.Find ("overlay").gameObject.GetComponent<Button>();
				btn.interactable = false;
				GameObject newSign = m.transform.Find ("new").gameObject;
				newSign.SetActive (false);
			}

		}

		GameObject m2 = levelPanel.transform.Find("mission1").gameObject;
		showSubButton(m2);

	}

	public void showSubButton(GameObject self)
	{

		GameObject levelPanel = GameObject.Find ("Levels");

		string stage = PlayerPrefs.GetString ("Stage");
		
		int c = int.Parse(stage.Substring (0, 1));
		int l = int.Parse(stage.Substring (2, 1));

		for(int i = 1; i < 6; i++)
		{
			GameObject m = levelPanel.transform.Find("mission" + i.ToString()).gameObject;
			if(m == self)
			{
				print (l + " " + i);

				GameObject stars = self.transform.Find ("stars").gameObject;
				stars.SetActive (false);

				GameObject selected = self.transform.Find ("selected").gameObject;
				selected.SetActive (true);

				if(i > l && Global.chapter >= c){
					Button btn = selected.transform.Find("Button").gameObject.GetComponent<Button>();

					btn.interactable = false;
				}
				else{
					Button btn = selected.transform.Find("Button").gameObject.GetComponent<Button>();
					
					btn.interactable = true;
				}

			}
			else
			{
				//GameObject stars = m.transform.Find ("stars").gameObject;
				//stars.SetActive (true);
				GameObject selected = m.transform.Find ("selected").gameObject;
				selected.SetActive (false);
			}
		}

		int offset = (Global.chapter - 1) * 5 + Global.level;

		int lv = recom_lv [offset - 1];
		
		if(lv < 10)
		{
			recom_ten.sprite = Resources.Load <Sprite>("ChapterUI/建議等級/NUMBERS[size_175]/0");
			recom_unit.sprite = Resources.Load <Sprite>("ChapterUI/建議等級/NUMBERS[size_175]/" + lv);
		}
		else{
			recom_ten.sprite = Resources.Load <Sprite>("ChapterUI/建議等級/NUMBERS[size_175]/" + lv/10);
			recom_unit.sprite = Resources.Load <Sprite>("ChapterUI/建議等級/NUMBERS[size_175]/" + lv%10);
		}



	}

	public void ShowAfter(int level){

		content.text = StoryReader.getStoryData ((Global.chapter - 1)*5 + (level - 1))["Ending"];
		After.SetActive (true);
		chapter_panel.SetActive (false);
		level_panel.SetActive (false);

	}

	public void Dismiss(){
		chapter_panel.SetActive (true);
		level_panel.SetActive (true);
		After.SetActive (false);
	}

}
