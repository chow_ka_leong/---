﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Story : MonoBehaviour {

	public string[] dialogue;
	public string[] names;
	public Image char1;
	public Image char2;

	public Sprite[] chars;
	public Text content;
	public Text name;

	public string opening;
	public string ending;


	public GameObject After;
	public Text After_content;

	Dictionary<string, string> story;

	int step = 0;

	// Use this for initialization
	void Start () {

		string scenename = Application.loadedLevelName.Replace ("Level", "");
		string chapter = scenename.Substring (0, 1);
		string level = scenename.Substring (2, 1);

		story = StoryReader.getStoryData ((int.Parse (chapter) - 1)*5 + int.Parse (level) - 1);

		name.text = "旁白";
		//print (story["Opening"].Substring(5, story["Opening"].Length-5));
		//content.text = story["Opening"].Substring(2, story["Opening"].Length-2);
		name.fontSize = Mathf.RoundToInt (14 * Screen.dpi / 323f);
		content.fontSize = Mathf.RoundToInt (24 * Screen.dpi / 323f);

		name.resizeTextForBestFit = true;
		content.resizeTextForBestFit = true;


		Next ();

	}

	void Update(){

		if(char1.sprite == null){
			char1.enabled = false;
		}
		else{
			char1.enabled = true;
		}

		if(char2.sprite == null){
			char2.enabled = false;
		}
		else{
			char2.enabled = true;
		}

	}

	
	public void Next(){

		if(step-1  == names.Length){

			gameObject.SetActive(false);
			Time.timeScale = 1;
			return;

		}

		if(step == 0){
			content.text = story["dialogue0"];
			step++;
			return;
		}

		string _name = story["char"+ step];



		name.text =_name.Substring (0, _name.IndexOf ("（"));

		if(_name.IndexOf ("（")+1+6 < _name.Length){
			if((step-1) % 2 == 0){
				char1.sprite = Resources.Load<Sprite> ("TeamUI/CHAR/" + _name.Substring (_name.IndexOf ("（")+1,6));
			}
			else{
				char2.sprite = Resources.Load<Sprite> ("TeamUI/CHAR/" + _name.Substring (_name.IndexOf ("（")+1, 6));
			}

		}

		content.text = story["dialogue" + step];
		step++;



	}

	public void End(){
		Time.timeScale = 0;
		After.SetActive (true);
		After_content.resizeTextForBestFit = true;
		After_content.fontSize = Mathf.RoundToInt (36f * Screen.dpi / 323f);
		After_content.text = content.text = story["Ending"];

//		Time.timeScale = 0;
//
//		char1.sprite = null;
//		char2.sprite = null;
//
//		name.text = "旁白";
//		content.text = content.text = story["Ending"];;

	}
}
