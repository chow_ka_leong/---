﻿using UnityEngine;
using System.Collections;

public class Warning : MonoBehaviour {

	float target = 0.51f;
	bool isCenter = false;



	void Update(){
		
		Vector3 pos = transform.position;

		pos.x -= Time.deltaTime / 2.0f;
		if(pos.x <= target && !isCenter)
		{

			pos.x = target;

			StartCoroutine(MoveLeft(2));
		}

		if(pos.x <= target && isCenter)
		{
			
			Destroy(gameObject);
		}
		
		transform.position = pos;
		
		
	}

	IEnumerator MoveLeft(float waitTime){
		yield return new WaitForSeconds(waitTime);
		isCenter = true;
		target = -2f;
	}


}
