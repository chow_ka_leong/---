﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ButtonEvent : MonoBehaviour,  IPointerDownHandler, IPointerUpHandler {

	public static bool mouseDown;
	public float timeMouseDown;


	public void OnPointerDown(PointerEventData eventData){
		mouseDown = true;
	}
	public void OnPointerUp(PointerEventData eventData){
		mouseDown = false;
		timeMouseDown = 0;

	}
	

	// Update is called once per frame
	void Update () {

		if(mouseDown){
			timeMouseDown += Time.deltaTime;

			if(timeMouseDown > 1)
			{
				//View Details
				transform.position = new Vector3(0,0,0);
			}
		}

	}


}