﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerManager : MonoBehaviour {

	float[] health = {100,100};

	public GameObject player1;
	public GameObject player2;


	GuiButton changebtn;

	public static string playerInActive;

	public int extraEXP = 0;
	public int extraMoney = 0;

	bool isGameOver = false;

	void Start ()
	{
		player1.SetActive (true);


		GameObject changeobj = GameObject.Find ("changeButton") as GameObject;
		changebtn = changeobj.GetComponent ("GuiButton") as GuiButton;

		playerInActive = Global.player_1_id;

		if(player2)
			player2.SetActive (false);
		else{
			health[1] = 0;
			changebtn.SetEnable(false);
		}
			
	}

	void Update () {

		if (changebtn.GetButtonDown ()) {

			if(player2 == null)
				return;

			if(player1.activeSelf){
				SwitchPlayer(player1, player2);
			}
			else{
				SwitchPlayer(player2, player1);
			}
			
		}

		if(health[0] == 0 && health[1] == 0 && !isGameOver){
			isGameOver = true;
			StartCoroutine(GameOver(2.0f));

		}

	}

	IEnumerator GameOver(float waitTime){
		yield return new WaitForSeconds(waitTime);
		print ("GameOver");
		//Time.timeScale = 0;
		GameObject gui = GameObject.Find("UI");
		gui.SetActive (false);
		
		GameObject joy = GameObject.Find("joystick");
		joy.SetActive (false);

		GameObject gameover = GameObject.Find("GameOver");
		gameover.transform.localScale = new Vector3 (1, 1, 1);

		StartCoroutine(Clean(2.0f));

	}

	IEnumerator Clean(float waitTime){
		yield return new WaitForSeconds(waitTime);
		Application.LoadLevel("SelectChapter");
		
	}



	void SwitchPlayer(GameObject old_p, GameObject new_p)
	{
		PlayerMovement status = old_p.GetComponent <PlayerMovement> ();
		PlayerMovement status_new = new_p.GetComponent <PlayerMovement> ();

		if (status.getStatus() != "Idle")
			return;
		if(status_new.currentState.ToString() == "Ground")
			return;

		if(player1.activeSelf){
			playerInActive = Global.player_2_id;
		}
		else{
			playerInActive = Global.player_1_id;
		}

		new_p.SetActive(true);
		new_p.transform.position = old_p.transform.position;
		new_p.transform.rotation = old_p.transform.rotation;
		old_p.SetActive(false);


	}

	public void SwitchPlayer(){


		GameObject old_p, new_p;
		if(player1.activeSelf){
			old_p = player1;
			new_p = player2;

			playerInActive = Global.player_2_id;
		}
		else{
			old_p = player2;
			new_p = player1;

			playerInActive = Global.player_1_id;
		}

		if(!new_p)
			return;

		new_p.SetActive(true);
		new_p.transform.position = old_p.transform.position;
		new_p.transform.rotation = old_p.transform.rotation;
		old_p.SetActive(false);


	}

	public void updateHealth(float val)
	{
		int index = (player1.activeSelf) ? 0 : 1;
		health[index] += val;

		if (health[index] < 1)
		       health[index] = 0;
		if(health[index] > 100)
		       health[index] = 100;



	}

	public void PlaySkill(PlayerMovement player, string ID)
	{
		SendMessage ("Skill" + ID, player);
	
	}

	public float getHealth()
	{
		int index = (player1.activeSelf) ? 0 : 1;
		return health[index];
	}

	public void setPlayers(GameObject p1, GameObject p2)
	{
		player1 = p1;
		player2 = p2;
	}

	public PlayerMovement getActivePlayerMovement(){

		if(playerInActive == Global.player_1_id)
			return player1.GetComponent<PlayerMovement>();
		else
			return player2.GetComponent<PlayerMovement>();

	}
}
