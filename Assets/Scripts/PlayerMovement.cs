﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public enum State { Idle, Walk, Dodge, Attack, Hit, Ground, Skill};
	enum Weapon {Hand , Sword, Spear, Bow};
	public enum Buff{None, AttackUp, SpeedUp, CritUp, DamageDown, Invincible};

	public float speed = 6f;            // The speed that the player will move at.
	public PlayerManager manager;
	public GameObject skill_fx;

	public int type_id;
	
	public GameObject weapon;

	public string ID;
	
	GameObject equipment;

	Vector3 movement;                   // The vector to store the direction of the player's movement.
	Animation anim;                      // Reference to the animator component.
	Rigidbody playerRigidbody;          // Reference to the player's rigidbody.
	int floorMask;                      // A layer mask so that a ray can be cast just at gameobjects on the floor layer.
	float camRayLength = 100f;          // The length of the ray from the camera into the scene.
	GameObject joy;
	Joystick js;

	GuiButton attackbtn;
	GuiButton dodgebtn;
	GuiButton skillbtn;

	int animation_step_index = 0;
	string[] animation_step = {"A", "B", "C", "D", "E"};
	string	animation_name;
	float animation_interval = 1.0f;
	float lastTap = 0;
	bool readyToAttack = true;

	float stun = 0.5f;

	private Vector3 cam_velocity = Vector3.zero;

	private SphereCollider sphere;

	public State currentState;

	public Buff currentBuff = Buff.None;

	private  float hp;
	private  float atk;

	public  float HP
	{
		get
		{
			return hp;
		}
		set
		{
			hp = value;
		}
	}

	public  float ATK
	{
		get
		{
			return atk;
		}
		set
		{
			atk = value;
		}
	}

	void Start ()
	{
		floorMask = LayerMask.GetMask ("Floor");

		anim = this.GetComponent <Animation> ();
		playerRigidbody = this.GetComponent <Rigidbody> ();

		joy = GameObject.Find ("guiL3controller") as GameObject;
		js = joy.GetComponent ("Joystick") as Joystick;

		GameObject atkobj = GameObject.Find ("attackButton") as GameObject;
		attackbtn = atkobj.GetComponent ("GuiButton") as GuiButton;

		GameObject dodgeobj = GameObject.Find ("dodgeButton") as GameObject;
		dodgebtn = dodgeobj.GetComponent ("GuiButton") as GuiButton;

		GameObject skillobj = GameObject.Find ("skillButton") as GameObject;
		skillbtn = skillobj.GetComponent ("GuiButton") as GuiButton;

		animation_name = "Attack_" + (Weapon)type_id + "_";

		manager = GameObject.Find ("PlayerManager").GetComponent <PlayerManager> ();

		Equip();

		sphere = GetComponent<SphereCollider> ();


	}

	void Update () {
		switch (currentState) {
		
		case State.Idle:
			Idle();
			CheckWalk();
			CheckDodge();
			CheckAttack();
			CheckSkill();
			break;
			
		case State.Walk:
			Walk();
			CheckIdle();
			CheckWalk();
			CheckDodge();
			CheckAttack();
			CheckSkill();
			break;
			
		case State.Dodge:

			break;
			
		case State.Attack:

			break;

		case State.Hit:

			break;	
		
		case State.Ground:
			
			break;	

		case State.Skill:
			
			break;	
		}


		UpdateCamera ();

		//hp = manager.getHealth ();
	}


	
	void Idle() {
		currentState = State.Idle;
		GetComponent<Animation>().CrossFade("Idle1h");
	}

	void CheckWalk(){
		if (js.GetXAxis () != 0 || js.GetYAxis () != 0) {

			currentState = State.Walk;

		}
	}

	void CheckDodge(){

		if (dodgebtn.GetButtonDown ()) {
			Dodge ();
			currentState = State.Dodge;
			
		}
	}

	void CheckAttack(){

		if (attackbtn.GetButtonDown ()) {
			Attack();
			currentState = State.Attack;

		}

	}

	void CheckSkill(){

		if (skillbtn.GetButtonDown ()) {
			currentState = State.Skill;
			skillbtn.SetEnable(false);
			Skill();

			
		}

	}


	void CheckIdle(){

		if ((js.GetXAxis () == 0 && js.GetYAxis () == 0) && !attackbtn.GetButton ())
				Idle ();

	}

	void Dodge(){
		//Dodge
		anim.Play ("Roll");
		playerRigidbody.AddForce (transform.forward * 400, ForceMode.Impulse);
		StartCoroutine(WaitForIdle(anim["Roll"].length));

	}

	void Skill()
	{
		anim.Play ("skill");
		//Instantiate (skill_fx, transform.position, transform.rotation);
		StartCoroutine(WaitForIdle(anim["skill"].length));
		StartCoroutine(CD(8.0f));
		manager.PlaySkill (this, ID);

	}

	IEnumerator CD(float waitTime){
		yield return new WaitForSeconds(waitTime);
		skillbtn.SetEnable(true);
		
	}

	void Attack(){
		if (lastTap == 0)
			lastTap = Time.timeSinceLevelLoad;

		float now = Time.timeSinceLevelLoad;

		if (now - lastTap < animation_interval) {

			if(animation_step_index < animation_step.Length - 1)
				animation_step_index++;
			else
				animation_step_index = 0;

		}
		else{
			animation_step_index = 0;
		}

		lastTap = now;

		string want_to_play = animation_name + animation_step [animation_step_index];

		if((type_id == 2 || type_id == 3) && animation_step_index < animation_step.Length - 1)
		{
			want_to_play += "_Start";
		}

		readyToAttack = false;
		anim.Play ( want_to_play );

		GetComponent<AudioSource>().Play ();

		FindEnemy ();

		updateAttackArea ();

		if(animation_step_index < animation_step.Length - 1)
			StartCoroutine(WaitForAttackFinished(anim[want_to_play].length));
		else
			StartCoroutine(WaitForIdle(anim[want_to_play].length));

		switch((Weapon)type_id)
		{
			case Weapon.Hand:
				transform.Translate (Vector3.forward * 0.35f);
			Instantiate(Resources.Load("FX/Attack_Hand_FX"), equipment.transform.position, transform.rotation);
			break;

			case Weapon.Sword:
				Instantiate(Resources.Load("FX/Attack_Sword_FX"), equipment.transform.position, Quaternion.identity);
			break;

			case Weapon.Spear:
			break;

			case Weapon.Bow:
				GameObject arrow = (GameObject)Instantiate(Resources.Load("Prefabs/Arrow"), transform.position + transform.up * 1, transform.rotation);
				Arrow info = arrow.GetComponent<Arrow>();
				info.setATK(atk);
				arrow.GetComponent<Rigidbody>().AddForce(transform.forward*300.0f);
			break;

		}

	}

	void updateAttackArea(){

		AttackArea.AreaType[] ForHand = {AttackArea.AreaType.Single, AttackArea.AreaType.Single, AttackArea.AreaType.Single, AttackArea.AreaType.Single, AttackArea.AreaType.Around};
		AttackArea.AreaType[] ForSpear = {AttackArea.AreaType.Single, AttackArea.AreaType.Single, AttackArea.AreaType.FrontSemiSphere, AttackArea.AreaType.Around, AttackArea.AreaType.FrontSemiSphere};
		AttackArea.AreaType[] ForSword = {AttackArea.AreaType.Single, AttackArea.AreaType.Single, AttackArea.AreaType.FrontSemiSphere, AttackArea.AreaType.FrontSemiSphere, AttackArea.AreaType.Around};

		switch((Weapon)type_id)
		{
		case Weapon.Hand:
			AttackArea.ChangeAttackAreaTo(sphere, ForHand[animation_step_index]);
			break;
			
		case Weapon.Sword:
			AttackArea.ChangeAttackAreaTo(sphere, ForSword[animation_step_index]);
			break;
			
		case Weapon.Spear:
			AttackArea.ChangeAttackAreaTo(sphere, ForSpear[animation_step_index]);
			break;

		}

	}

	IEnumerator WaitForAttackFinished(float waitTime){
		yield return new WaitForSeconds(waitTime);
		readyToAttack = true;
		currentState = State.Idle;
		//anim.CrossFade(animation_name + animation_step[animation_step_index] + "_End",0.2f);
		anim.Play(animation_name + animation_step[animation_step_index] + "_End");
		AttackArea.ChangeAttackAreaTo (sphere, AttackArea.AreaType.Single);
		//StartCoroutine(WaitForIdle(anim[animation_name + animation_step[animation_step_index] + "_End"].length));
	}

	IEnumerator WaitForIdle(float waitTime){
		yield return new WaitForSeconds(waitTime);
		currentState = State.Idle;
		if(sphere)
			AttackArea.ChangeAttackAreaTo (sphere, AttackArea.AreaType.Single);
		if(!readyToAttack)
			readyToAttack = true;
	}

	void Walk(){

		float buff = (currentBuff == Buff.SpeedUp)?7f:6f;

		speed = buff;

		Move (js.GetXAxis(), js.GetYAxis());
		anim.CrossFade("Run");
		Turning ();
	}

	void Hit(int val)
	{
		if(currentState == State.Dodge || currentBuff == Buff.Invincible)
			return;

		float buff = (currentBuff == Buff.DamageDown)?0.8f:1f;

		manager.updateHealth (val/HP*100f * -1 * buff);

		if(manager.getHealth() > 0)
		{
			currentState = State.Hit;
			anim.Play ("Hitted");
			Instantiate(Resources.Load("FX/Hitted_FX"),transform.position + transform.up * 1,transform.rotation);
			StartCoroutine(WaitForIdle(stun));

		}
		else
		{
			currentState = State.Ground;
			anim.Play ("Die");

			StartCoroutine(AfterDie(3));
		}

	}

	IEnumerator AfterDie(float waitTime){
		yield return new WaitForSeconds(waitTime);

		manager.SwitchPlayer ();
	}

	void OnCollisionStay(Collision col)
	{

		if(currentState == State.Idle || currentState == State.Walk)
		{

			if(col.gameObject.tag == "Enemy")
			{

				EnemyAI enemy = col.gameObject.GetComponent<EnemyAI>();
				print (enemy.getStatus());
				if(enemy.getStatus() == "Attack")
				{
					if(currentState == State.Hit)
						return;

					Hit(enemy.getAttack());
				}
			}

			if(col.gameObject.tag == "Arrow_ENE")
			{

				Arrow_ENE info = col.gameObject.GetComponent<Arrow_ENE>();
				if(currentState == State.Hit)
					return;
				Hit(Mathf.RoundToInt(info.getATK() * Random.Range(0.7f,1.2f)));
				Destroy(col.gameObject);
			}

			if(col.gameObject.tag == "ItemPoint")
			{
				Instantiate(Resources.Load("Prefabs/WoodenBox"), col.gameObject.transform.position, col.gameObject.transform.rotation);
				Destroy(col.gameObject);

			}


			if(col.gameObject.tag == "Item")
			{
				Item item = col.gameObject.GetComponent<Item>();
				SendMessage("EatItem" + item.getID());

				Destroy(col.gameObject);

			}

		}
		else
		{
			return;
		}

	}
	
	public void Move (float h, float v)
	{

		// Set the movement vector based on the axis input.
		movement.Set (h, 0f, v);
		
		// Normalise the movement vector and make it proportional to the speed per second.
		movement = movement.normalized * speed * Time.deltaTime;
		
		// Move the player to it's current position plus the movement.
		playerRigidbody.MovePosition (transform.position + movement);


	}
	
	void Turning ()
	{
		if(movement != Vector3.zero)
			transform.rotation = Quaternion.LookRotation(movement, Vector3.up);

	}

	void FindEnemy ()
	{

		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");

		GameObject closest = null;

		float distance = Mathf.Infinity;

		foreach (GameObject enemy in enemies) {

			float current_distance = Vector3.Distance(enemy.transform.position, transform.position);

			if(current_distance < distance)
			{
				distance = current_distance;
				closest = enemy;
			}

		}

		GameObject[] boxes = GameObject.FindGameObjectsWithTag ("Box");
		
		GameObject nearest_item = null;
		
		float distance_to_item = Mathf.Infinity;
		
		foreach (GameObject box in boxes) {
			
			float current_distance = Vector3.Distance(box.transform.position, transform.position);
			
			if(current_distance < distance_to_item)
			{
				distance_to_item = current_distance;
				nearest_item = box;
			}
			
		}
		if(distance_to_item < distance)
			transform.rotation = Quaternion.LookRotation(nearest_item.transform.position - transform.position);
		else if(distance < 10)
			transform.rotation = Quaternion.LookRotation(closest.transform.position - transform.position);

	}

	void UpdateCamera()
	{
		//Camera.main.transform.position = new Vector3(playerRigidbody.position.x, playerRigidbody.position.y + 7, playerRigidbody.position.z - 10);
		Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, new Vector3(playerRigidbody.position.x, playerRigidbody.position.y + 8, playerRigidbody.position.z - 10),ref cam_velocity, 0.3f);
	}

	public string getStatus ()
	{
		return currentState.ToString();
	}

	void Equip(){

			

		Transform right_hand =  transform.FindChild ("Bip001").FindChild ("Bip001 Pelvis").FindChild ("Bip001 Spine").FindChild ("Bip001 Spine1").FindChild ("Bip001 Neck").FindChild ("Bip001 R Clavicle").FindChild ("Bip001 R UpperArm").FindChild ("Bip001 R Forearm").FindChild ("Bip001 R Hand").FindChild ("Bip001_Weapon Point");
		if(!right_hand)
			right_hand = transform.FindChild ("Bip001").FindChild ("Bip001 Pelvis").FindChild ("Bip001 Spine").FindChild ("Bip001 Spine1").FindChild ("Bip001 Neck").FindChild ("Bip001 R Clavicle").FindChild ("Bip001 R UpperArm").FindChild ("Bip001 R Forearm").FindChild ("Bip001 R Hand").FindChild ("Bip001_Weapon Point R");


		if(type_id == 3)
			right_hand = transform.FindChild ("Bip001").FindChild ("Bip001 Pelvis").FindChild ("Bip001 Spine").FindChild ("Bip001 Spine1").FindChild ("Bip001 Neck").FindChild ("Bip001 L Clavicle").FindChild ("Bip001 L UpperArm").FindChild ("Bip001 L Forearm").FindChild ("Bip001 L Hand").FindChild ("Bip001_Weapon Point L");
		if(!right_hand)
			right_hand = transform.FindChild ("Bip001").FindChild ("Bip001 Pelvis").FindChild ("Bip001 Spine").FindChild ("Bip001 Spine1").FindChild ("Bip001 Neck").FindChild ("Bip001 R Clavicle").FindChild ("Bip001 R UpperArm").FindChild ("Bip001 R Forearm").FindChild ("Bip001 R Hand").FindChild ("Bip001_Weapon Point");


		if(!weapon)
		{
			equipment = right_hand.gameObject;
			return;
		}


		equipment = Instantiate(weapon, transform.position, transform.rotation) as GameObject;	

		equipment.transform.position = right_hand.position;
		equipment.transform.rotation = right_hand.rotation;
		
		equipment.transform.parent = right_hand;

		Destroy (equipment.GetComponent<MeshCollider>());
		
	}

	void EatItem1 ()
	{
		manager.updateHealth (25);
		GameObject fx = (GameObject)Instantiate(Resources.Load("FX/HP_Healing25"), transform.position, transform.rotation);
		fx.transform.parent = transform;
		StartCoroutine(DeBuff(1, fx));
		
	}

	void EatItem2 ()
	{
		manager.updateHealth (50);
		GameObject fx = (GameObject)Instantiate(Resources.Load("FX/HP_Healing50"), transform.position, transform.rotation);
		fx.transform.parent = transform;
		StartCoroutine(DeBuff(1, fx));
		
	}

	void EatItem3 ()
	{
		manager.updateHealth (100);
		GameObject fx = (GameObject)Instantiate(Resources.Load("FX/HP_Healing100"), transform.position, transform.rotation);
		fx.transform.parent = transform;
		StartCoroutine(DeBuff(1, fx));
		
	}

	public void EatItem4(){
		currentBuff = Buff.AttackUp;
		GameObject fx = (GameObject)Instantiate(Resources.Load("FX/Attack_Up"), transform.position, transform.rotation);
		fx.transform.parent = transform;
		StartCoroutine(DeBuff(10, fx));
	}

	void EatItem5(){
		currentBuff = Buff.SpeedUp;
		GameObject fx = (GameObject)Instantiate(Resources.Load("FX/Speed_Up"), transform.position, transform.rotation);
		fx.transform.parent = transform;
		StartCoroutine(DeBuff(10, fx));
	}

	void EatItem6(){
		currentBuff = Buff.CritUp;
		GameObject fx = (GameObject)Instantiate(Resources.Load("FX/Crit_Up"), transform.position, transform.rotation);
		fx.transform.parent = transform;
		StartCoroutine(DeBuff(10, fx));
	}

	public void EatItem7(){
		currentBuff = Buff.DamageDown;
		GameObject fx = (GameObject)Instantiate(Resources.Load("FX/Hit_Down"), transform.position, transform.rotation);
		fx.transform.parent = transform;
		StartCoroutine(DeBuff(10, fx));
	}

	void EatItem8(){
		currentBuff = Buff.Invincible;
		GameObject fx = (GameObject)Instantiate(Resources.Load("FX/Invincible"), transform.position, transform.rotation);
		fx.transform.parent = transform;
		StartCoroutine(DeBuff(10, fx));
	}

	void EatItem9(){
		manager.extraEXP += 1000;
	}

	void EatItem10(){
		manager.extraMoney += 500;
	}

	IEnumerator DeBuff(float waitTime, GameObject fx){
		yield return new WaitForSeconds(waitTime);
		currentBuff = Buff.None;
		Destroy (fx);
	}
}
