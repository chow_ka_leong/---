﻿using UnityEngine;
using System.Collections;

public class AttackArea : MonoBehaviour {

	public enum AreaType{Single, FrontSemiSphere, Around};

	static float factor = 1.15f;

	public static void ChangeAttackAreaTo(SphereCollider sphere, AttackArea.AreaType type){

		if(!sphere)
			return;

		if(type == AreaType.Single){
			sphere.center = new Vector3 (0, 1, 1);
			sphere.radius = 0.49f * factor;
		}

		if(type == AreaType.FrontSemiSphere){
			sphere.center = new Vector3 (0, 1, 1);
			sphere.radius = 0.79f * factor;
		}

		if(type == AreaType.Around){
			sphere.center = new Vector3 (0, 1, 0);
			sphere.radius = 1.2f * factor;
		}

	}
}

