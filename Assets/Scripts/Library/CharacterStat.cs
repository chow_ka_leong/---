﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class CharacterStat : MonoBehaviour {

	static int offset = 12;
	
	private static string[] data;
	
	public static string[] Data
	{
		get
		{
			if(data == null)data = CSVReader.ParseCsvRow ("CSV/CharacterStat");
			return data;
		}
	}
	
	public static Dictionary<string, string> getCharacterData (string ID)
	{
		Dictionary<string, string> dict = new Dictionary<string , string>();
		
		int row = int.Parse (ID);

//		if(row < 10){
//			ID = "0"+ID;
//		}

		for(int i=0; i < Data.Length; i++) 
		{
			if(Data[i] == "SKY0" + ID)
				row = i;
		}

		dict ["ID"] = Data [ row];
		dict ["name"] = Data [ row + 1];
		dict ["job"] = Data [ row + 2];
		dict ["type"] = Data [ row + 3];
		dict ["rare"] = Data [ row + 4];
		dict ["upgrade_type"] = Data [ row + 5];
		dict ["vit"] = Data [ row + 6];
		dict ["str"] = Data [ row + 7];
		dict ["total"] = Data [ row + 8];
		dict ["hp"] = Data [ row + 9];
		dict ["atk"] = Data [ row + 10];
		dict ["speed"] = Data [ row + 11];
		
		return dict;
		
	}
}
