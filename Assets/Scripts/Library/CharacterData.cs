﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System.IO;

public class CharacterData : MonoBehaviour {

	static int offset = 6;

	private static string[] data;

	public static string[] Data
	{
		get
		{
			if(data == null)data = CSVReader.ParseCsvRow ("CSV/CharacterData");
			return data;
		}
	}

	public static Dictionary<string, string> getCharacterData (string ID)
	{
		Dictionary<string, string> dict = new Dictionary<string , string>();

		int row = int.Parse (ID);


		dict ["ID"] = Data [offset * row];
		dict ["name"] = Data [offset * row + 1];
		dict ["gender"] = Data [offset * row + 2];
		dict ["bible"] = Data [offset * row + 3];
		dict ["design"] = Data [offset * row + 4];
		dict ["background"] = Data [offset * row + 5];

		return dict;

	}


	

}
