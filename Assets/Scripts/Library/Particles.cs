﻿using UnityEngine;
using System.Collections;

public class Particles : MonoBehaviour {

	public bool isDestroyImmediately = true;

	
	void OnParticleCollision(GameObject test) {
		if(isDestroyImmediately)
			Destroy (gameObject.transform.parent.gameObject);

		print ("Collsion from : " + gameObject.name);
		if(test.tag == "Enemy"){

			EnemyAI ai = test.GetComponent<EnemyAI> ();
			if(ai.getStatus() != "Die" && ai.getStatus() != "Hit")
			{

				PlayerManager manager = GameObject.Find("PlayerManager").GetComponent<PlayerManager>();
				PlayerMovement player = manager.getActivePlayerMovement();


				float buff = (player.currentBuff == PlayerMovement.Buff.AttackUp)?1.2f:1f;
				ai.hit(Mathf.RoundToInt(player.ATK * Random.Range(0.7f,1.2f) * buff));
			}

		}
	}
}
