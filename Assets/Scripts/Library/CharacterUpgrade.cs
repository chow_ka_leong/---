﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class CharacterUpgrade : MonoBehaviour {

	private static string[] data;
	
	public static string[] Data
	{
		get
		{
			if(data == null)data = CSVReader.ParseCsvRow ("CSV/CharacterUpgrade");
			return data;
		}
	}
	
	
	public static Dictionary<string, string> getCharData (int level)
	{
		Dictionary<string, string> dict = new Dictionary<string , string>();
		//level++;
		
		int index = 0;
		
		for(int i=0; i < Data.Length; i++) 
		{
			int tmp = 0;
			if(int.TryParse(Data[i], out tmp))
			{
				if(int.Parse(Data[i]) == level)
					index = i;
			}
		}

		dict ["hp"] = Data [index + 1];
		dict ["atk"] = Data [index + 2];
		
		return dict;
		
	}
}
