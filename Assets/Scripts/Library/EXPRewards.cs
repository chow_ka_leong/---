﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;


public class EXPRewards : MonoBehaviour {
	private static string[] data;
	
	public static string[] Data
	{
		get
		{
			if(data == null)data = CSVReader.ParseCsvRow ("CSV/MissionRewards_EXP");
			return data;
		}
	}
	
	public static int getMissionData (string level)
	{


		int index = 0;
		
		for(int i=0; i < Data.Length; i++) 
		{
			if(Data[i] == level)
				index = i;
		}

		return int.Parse( Data[index+1]);
		
	}
}
