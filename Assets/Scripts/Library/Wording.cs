﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Wording : MonoBehaviour {

	private static string[] data;
	
	public static string[] Data
	{
		get
		{
			if(data == null)data = CSVReader.ParseCsvRow ("CSV/Wording");
			return data;
		}
	}

}
