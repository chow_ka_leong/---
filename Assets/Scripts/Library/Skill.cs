﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Skill : MonoBehaviour {

	static int offset = 12;
	
	private static string[] data;
	
	public static string[] Data
	{
		get
		{
			if(data == null)data = CSVReader.ParseCsvRow ("CSV/Skill");
			return data;
		}
	}
	
	public static Dictionary<string, string> getSkill (string ID)
	{
		Dictionary<string, string> dict = new Dictionary<string , string>();
		
		int row = int.Parse (ID);

		for(int i=0; i < Data.Length; i++) 
		{
			if(Data[i] == "SKY0" + ID)
				row = i;
		}

		dict ["ID"] = Data [ row];
		dict ["name"] = Data [ row + 1];
		dict ["info"] = Data [ row + 3];
		
		return dict;
		
	}
}
