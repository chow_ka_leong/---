﻿
using UnityEngine;
using System.Collections;

public class EXPChart : MonoBehaviour {

	private static string[] data;
	
	public static string[] Data
	{
		get
		{
			if(data == null)data = CSVReader.ParseCsvRow ("CSV/EXPChart");
			return data;
		}
	}
	

	public static int getEXPData (int level)
	{
		level++;

		int index = 0;

		for(int i=0; i < Data.Length; i++) 
		{
			if(int.Parse(Data[i]) == level)
				index = i;
		}
		
		return int.Parse( Data[index+1]);
		
	}
}
