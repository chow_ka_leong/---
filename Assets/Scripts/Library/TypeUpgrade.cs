﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class TypeUpgrade : MonoBehaviour {

	private static string[] data;
	
	public static string[] Data
	{
		get
		{
			if(data == null)data = CSVReader.ParseCsvRow ("CSV/TypeUpgrade");
			return data;
		}
	}
	
	
	public static Dictionary<string, string> getCharData (string type)
	{
		Dictionary<string, string> dict = new Dictionary<string , string>();

		
		int index = 0;
		
		for(int i=0; i < Data.Length; i++) 
		{
			if(Data[i] == type)
				index = i;
		}

		dict ["hp"] = Data [index + 1];
		dict ["atk"] = Data [index + 2];
		
		return dict;
		
	}
}
