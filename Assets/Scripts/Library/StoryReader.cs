﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class StoryReader : MonoBehaviour {

	private static string[] data;

	static int offset = 12;
	
	public static string[] Data
	{
		get
		{
			if(data == null)data = CSVReader.ParseCsvComa ("CSV/Story");
			return data;
		}
	}
	
	public static Dictionary<string, string> getStoryData (int row)
	{
		Dictionary<string, string> dict = new Dictionary<string , string>();

		dict ["Opening"] = Data [offset * row];
		dict ["dialogue0"] = Data [offset * row + 2];
		dict ["char1"] = Data [offset * row + 3];
		dict ["dialogue1"] = Data [offset * row + 4];
		dict ["char2"] = Data [offset * row + 5];
		dict ["dialogue2"] = Data [offset * row + 6];
		dict ["char3"] = Data [offset * row + 7];
		dict ["dialogue3"] = Data [offset * row + 8];
		dict ["char4"] = Data [offset * row + 9];
		dict ["dialogue4"] = Data [offset * row + 10];
		dict ["Ending"] = Data [offset * row + 11];
		return dict;
		
	}
}
