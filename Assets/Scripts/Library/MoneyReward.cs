﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;


public class MoneyReward : MonoBehaviour {
	private static string[] data;
	
	public static string[] Data
	{
		get
		{
			if(data == null)data = CSVReader.ParseCsvRow ("CSV/MissionRewards_Money");
			return data;
		}
	}
	
	public static string getMissionData (string level)
	{


		int index = 0;
		
		for(int i=0; i < Data.Length; i++) 
		{
			if(Data[i] == level)
				index = i;
		}

		return Data[index+1];
		
	}
}
