﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TeamUI : MonoBehaviour {

	public Image showChar;
	public Image primaryIcon;
	public Image secondaryChar;

	public GameObject main;
	public GameObject select;

	public RectTransform scrollView;
	public GridLayoutGroup grid;
	public ScrollRect scroll;
	
	public Global global;

	public GameObject char_template;

	private int selectedIndex = 0;

	public Text skill;
	public Text bg;

	public Text hp;
	public Text atk;
	public Image lv_ten;
	public Image lv_unit;

	Vector3 visible = new Vector3(1,1,1);
	Vector3 invisible = new Vector3(0,0,0);

	[SerializeField] GameObject template;

	void Awake()
	{
//		string s = "";
//		for(int i=1;i<44;i++){
//			if(i < 10){
//				s += "PlayerPrefsX.SetStringArray(\"Char0"+i+"\", new string[]{\"1\", \"0\"});\n";
//			}
//			else{
//				s += "PlayerPrefsX.SetStringArray(\"Char"+i+"\", new string[]{\"1\", \"0\"});\n";
//			}
//			
//		}
//		
//		print (s);

		string player1 = global.getPlayerID (0);
		string player2 = global.getPlayerID (1);
		
		showChar.sprite = Resources.Load<Sprite> ("TeamUI/角色card/®§¶‚card_SKY0" + player1);
		primaryIcon.sprite = Resources.Load<Sprite> ("TeamUI/CHAR_ICON/CHAR_ICON_SKY0" + player1);
		if(player2 != "")
		secondaryChar.sprite = Resources.Load<Sprite> ("TeamUI/CHAR_ICON/CHAR_ICON_SKY0" + player2);


		showData  (player1);
		showLevel (player1);


	}

	public void changeMain(int index)
	{
		selectedIndex = index;
		string ID = global.getPlayerID (index);

		if (ID == "")
			return;

		showChar.sprite = Resources.Load<Sprite> ("TeamUI/角色card/®§¶‚card_SKY0" + ID);


		showData (ID);
		showLevel (ID);


	}

	public void selectCharacter()
	{
		if(select.activeSelf)
			return;

		main.SetActive (false);
		select.SetActive (true);

		generateList ();
	}

	void generateList()
	{

		RectTransform itemRect = template.GetComponent<RectTransform> ();

		grid.cellSize = new Vector2 (itemRect.rect.width, itemRect.rect.height);

		float guiRatio = (float)Screen.height/640.0f;  
		//create a scale Vector3 with the above ratio  
		Vector3 GUIsF = new Vector3(guiRatio,guiRatio,1);  

		string[] str = PlayerPrefsX.GetStringArray ("PlayerList");

		int index = 0;


		foreach(string ID in str)//global.getPlayerList())
		{
			string capturedID = ID;
			if(capturedID == global.getPlayerID(0) || capturedID == global.getPlayerID(1))
				continue;

			float offsetx = 0;

			offsetx = (index%2 == 0)?-75 * GUIsF.x:45  * GUIsF.x;

			Dictionary<string, string> dict = CharacterStat.getCharacterData(capturedID);

			GameObject nButton = Instantiate(template, new Vector2(offsetx,-(index/2) * 130 * GUIsF.y), Quaternion.identity) as GameObject;
			RectTransform r = nButton.GetComponent<RectTransform>();
			nButton.SetActive(true);
			float capturedHeight  = r.rect.height;

			nButton.transform.SetParent(scrollView, false);

			Image img = nButton.transform.Find("tmp_char").gameObject.GetComponent("Image") as Image;
			img.sprite = Resources.Load<Sprite> ("TeamUI/CHAR_ICON/CHAR_ICON_SKY0" + ID);
			Button btn = nButton.transform.Find("tmp_char").gameObject.GetComponent("Button") as Button;

			Image weapon = nButton.transform.Find("tmp_weapon").gameObject.GetComponent("Image") as Image;
			weapon.sprite = Resources.Load<Sprite> ("TeamUI/CHAR_ICON_SMALL/CHAR_ICON_SMALL_" + dict ["type"]);
			for(int i=5; i > int.Parse(dict ["rare"]); i-- ){

				GameObject star = nButton.transform.Find("tmp_rare").Find("star_" + i).gameObject;
				star.SetActive(false);
			}

			btn.onClick.AddListener(() => {
				if(selectedIndex == 0)
				{
					print ("TeamUI/CHAR_ICON/CHAR_ICON_SKY0" + capturedID);
					global.setPlayer1(capturedID);
					primaryIcon.sprite = Resources.Load<Sprite> ("TeamUI/CHAR_ICON/CHAR_ICON_SKY0" + capturedID);
				}
					
				else
				{
					global.setPlayer2(capturedID);
					secondaryChar.sprite = Resources.Load<Sprite> ("TeamUI/CHAR_ICON/CHAR_ICON_SKY0" + capturedID);
				}
					
				//generateList();
				showChar.sprite = Resources.Load<Sprite> ("TeamUI/角色card/®§¶‚card_SKY0" + capturedID);
				showLevel(capturedID);
				showData(capturedID);
				main.SetActive (true);
				select.SetActive (false);

				foreach(Transform child in scrollView) {
					Destroy(child.gameObject);
				}

			});



			index++;
		}
		scroll.normalizedPosition = new Vector2 (1, 1);



	}

	void showData(string ID){
		bg.text = CharacterData.getCharacterData (ID)["background"];
		skill.text = Skill.getSkill(ID)["name"] + " - " + Skill.getSkill(ID)["info"];
		
		hp.text = calHP (ID).ToString ();
		atk.text = calATK (ID).ToString ();
		
	}

	void showLevel(string ID){
		string[] data = PlayerPrefsX.GetStringArray ("Char" + ID);

		int level = int.Parse (data [0]);

		if(level < 10){
			lv_ten.sprite = Resources.Load<Sprite> ("TeamUI/角色card/number[size46x60]/0");
			lv_unit.sprite = Resources.Load<Sprite> ("TeamUI/角色card/number[size46x60]/" + level);
		}
		else{
			lv_ten.sprite = Resources.Load<Sprite> ("TeamUI/角色card/number[size46x60]/" + level/10);
			lv_unit.sprite = Resources.Load<Sprite> ("TeamUI/角色card/number[size46x60]/" + level%10);
		}

	}

	int calHP(string ID)
	{
		Dictionary<string, string> dict = CharacterStat.getCharacterData (ID);
		
		string[] character = PlayerPrefsX.GetStringArray("Char"+ID);
		
		float hp = float.Parse (dict ["hp"]);
		int level = int.Parse (character [0]);
		float final = (float.Parse (CharacterUpgrade.getCharData (level)["hp"]) + (hp - 1000.0f)/2) * (1 + 0.05f * float.Parse (dict ["rare"]) + float.Parse(TypeUpgrade.getCharData (dict ["upgrade_type"])["hp"]));
		
		if(level > 1)
			return Mathf.RoundToInt(final);
		else
			return Mathf.RoundToInt(hp);
	}
	
	int calATK(string ID)
	{
		Dictionary<string, string> dict = CharacterStat.getCharacterData (ID);
		
		string[] character = PlayerPrefsX.GetStringArray("Char"+ID);
		
		float atk = float.Parse (dict ["atk"]);
		int level = int.Parse (character [0]);
		float final = (float.Parse (CharacterUpgrade.getCharData (level)["atk"]) + (atk - 300.0f)/2) * (1 + 0.05f * float.Parse (dict ["rare"]) + float.Parse(TypeUpgrade.getCharData (dict ["upgrade_type"])["atk"]));
		
		if(level > 1)
			return Mathf.RoundToInt(final);
		else
			return Mathf.RoundToInt(atk);
	}
}
