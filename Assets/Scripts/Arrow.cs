﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour {

	public float atk = 0;

	// Use this for initialization
	void Start () {
		StartCoroutine (CleanUp (10.0f));
	}
	
	void OnTriggerStay(Collider col)
	{

		if(col.tag == "Enemy")
		{
			Destroy(this.gameObject);
			
			EnemyAI ai = col.gameObject.GetComponent<EnemyAI> ();
			if(ai.getStatus() != "Die" && ai.getStatus() != "Hit")
				ai.hit(Mathf.RoundToInt(atk * Random.Range(0.7f,1.2f)));
		}
		
		if(col.tag == "Box")
		{
			Destroy(this.gameObject);

			Box box = col.gameObject.transform.parent.gameObject.GetComponent<Box> ();
			box.Break();
		}
	}

	IEnumerator CleanUp(float waitTime){
		yield return new WaitForSeconds(waitTime);
		Destroy (this.gameObject);
	}

	public void setATK(float _a)
	{
		atk = _a;
	}
}
