﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour {

	string[] wording = {"使用左下方的搖杆進行移動", "使用攻擊鍵作出攻擊", "使用回避鍵脫離危險", "使用技能鍵作出特殊攻擊", "擁有隊友之下，使用轉換鍵，切換角色"};

	public Text desc;

	private int step = 0;

	public GuiButton attackBtn;
	public GuiButton dodgeBtn;
	public GuiButton skillBtn;
	public GuiButton changeBtn;

	// Use this for initialization
	void Start () {
		desc.fontSize = Mathf.RoundToInt (36f * Screen.dpi / 323f);

		desc.text = wording [step];

		attackBtn.SetEnable (false);
		dodgeBtn.SetEnable (false);
		skillBtn.SetEnable (false);
		changeBtn.SetEnable (false);
	}

	public void nextStep(){

		step++;

		switch(step){

			case 1:
			attackBtn.SetEnable (true);
			dodgeBtn.SetEnable (false);
			skillBtn.SetEnable (false);
			changeBtn.SetEnable (false);
			desc.text = wording [step];
				break;

			case 2:
			attackBtn.SetEnable (false);
			dodgeBtn.SetEnable (true);
			skillBtn.SetEnable (false);
			changeBtn.SetEnable (false);
			desc.text = wording [step];
				break;

			case 3:
			attackBtn.SetEnable (false);
			dodgeBtn.SetEnable (false);
			skillBtn.SetEnable (true);
			changeBtn.SetEnable (false);
			desc.text = wording [step];
				break;

			case 4:
			attackBtn.SetEnable (false);
			dodgeBtn.SetEnable (false);
			skillBtn.SetEnable (false);
			changeBtn.SetEnable (true);
			desc.text = wording [step];
				break;

			case 5:
			Application.LoadLevel("MainUI");
			break;

		}



	}
	

}
