﻿using UnityEngine;
using System.Collections;
using System;

public class Shop : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Unibiller.Initialise();
		Unibiller.onPurchaseCompleteEvent += onPurchased;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Closing(){
		gameObject.SetActive (false);
	}

	public void Buy3Diamond(){
		Unibiller.initiatePurchase("gem_03");

//		Unibiller.onBillerReady += (state) => {
//			if (UnibillState.SUCCESS == state) {
//				Unibiller.initiatePurchase("gem_03");
//			}
//		};
//		Unibiller.Initialise();
	}

	public void Buy18Diamond(){
		Unibiller.initiatePurchase("gem_18");
//		Unibiller.onBillerReady += (state) => {
//			if (UnibillState.SUCCESS == state) {
//				Unibiller.initiatePurchase("gem_18");
//			}
//		};
//		Unibiller.Initialise();
	}

	public void Buy36Diamond(){
		Unibiller.initiatePurchase("gem_36");
//		Unibiller.onBillerReady += (state) => {
//			if (UnibillState.SUCCESS == state) {
//				Unibiller.initiatePurchase("gem_36");
//			}
//		};
//		Unibiller.Initialise();
	}

	public void Buy90Diamond(){
		Unibiller.initiatePurchase("gem_90");
//		Unibiller.onBillerReady += (state) => {
//			if (UnibillState.SUCCESS == state) {
//				Unibiller.initiatePurchase("gem_90");
//			}
//		};
//		Unibiller.Initialise();
	}

	public void Buy180Diamond(){
		Unibiller.initiatePurchase("gem_180");
//		Unibiller.onBillerReady += (state) => {
//			if (UnibillState.SUCCESS == state) {
//				Unibiller.initiatePurchase("gem_180");
//			}
//		};
//		Unibiller.Initialise();
	}

	private void onPurchased(PurchaseEvent e) {

		Debug.Log ("Receipt: " + e.Receipt + " Item ID : " + e.PurchasedItem.Id);

		int d = PlayerPrefs.GetInt ("diamond");

		switch(e.PurchasedItem.Id){

		case "gem_03":
			PlayerPrefs.SetInt("diamond",d+3);
			break;
		case "gem_18":
			PlayerPrefs.SetInt("diamond",d+18);
			break;
		case "gem_36":
			PlayerPrefs.SetInt("diamond",d+36);
			break;
		case "gem_90":
			PlayerPrefs.SetInt("diamond",d+90);
			break;
		case "gem_180":
			PlayerPrefs.SetInt("diamond",d+180);
			break;

		}

		PlayerPrefs.Save();

//		Debug.Log("Purchase OK: " + e.PurchasedItem.Id);
//		Debug.Log ("Receipt: " + e.Receipt);
//		Debug.Log(string.Format ("{0} has now been purchased {1} times.",
//		                         e.PurchasedItem.name,
//		                         Unibiller.GetPurchaseCount(e.PurchasedItem)));
	}

//	public static event Action<PurchasableItem> onPurchaseComplete{
//
//
//	}
//
//	public static event Action<PurchasableItem> onPurchaseCancelled{
//		
//		
//	}
//
//	public static event Action<PurchasableItem> onPurchaseFailed{
//		
//		
//	}
}
