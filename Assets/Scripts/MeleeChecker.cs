﻿using UnityEngine;
using System.Collections;

public class MeleeChecker : MonoBehaviour {

	public PlayerMovement player;

	void OnTriggerStay(Collider col)
	{

		if(player.getStatus()== "Attack" && col.tag == "Enemy")
		{
			//Destroy(col.gameObject);

			EnemyAI ai = col.gameObject.GetComponent<EnemyAI> ();
			if(ai.getStatus() != "Die" && ai.getStatus() != "Hit")
			{
				float buff = (player.currentBuff == PlayerMovement.Buff.AttackUp)?1.2f:1f;
				ai.hit(Mathf.RoundToInt(player.ATK * Random.Range(0.7f,1.2f) * buff));
			}
				
		}

		if(player.getStatus()== "Attack" && col.tag == "Box")
		{
			Box box = col.gameObject.transform.parent.gameObject.GetComponent<Box> ();
			box.Break();
		}
	}

}
