﻿using UnityEngine;
using System.Collections;

public class wording : MonoBehaviour {

	float charactersPerSecond = 8.0f;

	private string scrollBasis;
	private string scrollText;

	private int currChar  = 0;
	private float timer  = 0.0f;

	private string[] wordings;
	private string textUsing;


	void Start(){
		GetComponent<GUIText>().fontSize = Mathf.RoundToInt (36f * Screen.dpi / 323f);
		wordings = Wording.Data;

		NewText();
	}

	void Update(){

		Vector3 pos = transform.position;
		pos.x -= Time.deltaTime / 10.0f;
		if(pos.x < -0.6)
		{
			pos.x = 1;
			NewText();
		}
			
		transform.position = pos;


	}

	void NewText(){
		textUsing = wordings [Random.Range (0, wordings.Length)];
		GetComponent<GUIText>().text = textUsing;
	}


}
