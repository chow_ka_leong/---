﻿using UnityEngine;
using System.Collections;

public class SkillSet : MonoBehaviour {

	private GameObject fx;
	private Transform transform;

	void Skill01(PlayerMovement player)
	{
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		fx.transform.parent = transform;
		StartCoroutine (Skill01_Post (0.3f, player));

	}

	IEnumerator Skill01_Post(float waitTime, PlayerMovement obj){
		yield return new WaitForSeconds(waitTime);
		obj.currentState = PlayerMovement.State.Attack;
		Rigidbody playerRigidbody = obj.GetComponent <Rigidbody> ();
		playerRigidbody.AddForce (obj.transform.forward * 300, ForceMode.Impulse);
		StartCoroutine (Skill01_After (1.7f, obj));
		StartCoroutine (ParticleCleaner (1.7f, fx));
	}

	IEnumerator Skill01_After(float waitTime, PlayerMovement obj){
		yield return new WaitForSeconds(waitTime);
		obj.currentState = PlayerMovement.State.Idle;
	}

	void Skill02(PlayerMovement player)
	{
		transform = player.transform;
		Instantiate (player.skill_fx, transform.position, Quaternion.identity);
		StartCoroutine (Skill02_Post (0.3f, player));
	}

	IEnumerator Skill02_Post(float waitTime, PlayerMovement obj){
		yield return new WaitForSeconds(waitTime);
		obj.currentState = PlayerMovement.State.Attack;
		SphereCollider sphere = obj.GetComponent<SphereCollider> ();
		AttackArea.ChangeAttackAreaTo (sphere, AttackArea.AreaType.Around);
		StartCoroutine (Skill02_After (1.7f, obj));
		StartCoroutine (ParticleCleaner (1.7f, fx));
	}

	IEnumerator Skill02_After(float waitTime, PlayerMovement obj){
		yield return new WaitForSeconds(waitTime);
		obj.currentState = PlayerMovement.State.Idle;
		SphereCollider sphere = obj.GetComponent<SphereCollider> ();
		AttackArea.ChangeAttackAreaTo (sphere, AttackArea.AreaType.Single);
	}


	void Skill03(PlayerMovement player)
	{
		transform = player.transform;
		GameObject fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation) ;
		StartCoroutine (ParticleCleaner (5.0f, fx));

	}

	void Skill05(PlayerMovement player){
		
		ArrowRain (player);
	}

	void Skill06(PlayerMovement player){
		
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		fx.transform.parent = transform;
		player.currentState = PlayerMovement.State.Attack;
		SphereCollider sphere = player.GetComponent<SphereCollider> ();
		AttackArea.ChangeAttackAreaTo (sphere, AttackArea.AreaType.Around);
		StartCoroutine (ParticleCleaner (5.0f, fx));
		StartCoroutine (Skill06_Post (5.0f, player));
	}

	IEnumerator Skill06_Post(float waitTime, PlayerMovement obj){
		
		yield return new WaitForSeconds(waitTime);
		obj.currentState = PlayerMovement.State.Idle;
		SphereCollider sphere = obj.GetComponent<SphereCollider> ();
		AttackArea.ChangeAttackAreaTo (sphere, AttackArea.AreaType.Single);
	}

	void Skill07(PlayerMovement player){
		
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		fx.transform.parent = transform;
		player.currentState = PlayerMovement.State.Attack;
		StartCoroutine (ParticleCleaner (5.0f, fx));
	}

	void Skill08(PlayerMovement player){
		
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		fx.transform.parent = transform;
		SphereCollider sphere = player.GetComponent<SphereCollider> ();
		AttackArea.ChangeAttackAreaTo (sphere, AttackArea.AreaType.FrontSemiSphere);
		StartCoroutine (ParticleCleaner (5.0f, fx));
	}


	void Skill09(PlayerMovement player){
		
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);

		GameObject fx2 = (GameObject)Instantiate (Resources.Load("FX/Sky_5Star_Skill_FX"), transform.position, transform.rotation);
		fx2.transform.parent = transform;
		StartCoroutine (Skill09_Post (0.5f, player));
		StartCoroutine (ParticleCleaner (5.0f, fx));
	}

	IEnumerator Skill09_Post(float waitTime, PlayerMovement obj){
		
		yield return new WaitForSeconds(waitTime);
		obj.currentState = PlayerMovement.State.Attack;
		SphereCollider sphere = obj.GetComponent<SphereCollider> ();
		AttackArea.ChangeAttackAreaTo (sphere, AttackArea.AreaType.Around);
	}


	void Skill10(PlayerMovement player){
		
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		fx.transform.parent = transform;

		player.manager.updateHealth (25);
	}

	void Skill11(PlayerMovement player){
		
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		
		StartCoroutine (ParticleCleaner (5.0f, fx));
	}

	void Skill12(PlayerMovement player){
		
		ArrowRain (player);
	}

	void Skill13(PlayerMovement player){

		ArrowRain (player);
	}

	void Skill14(PlayerMovement player){
		
		Skill07 (player);
	}

	void Skill15(PlayerMovement player){
		
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		fx.transform.parent = transform;
		GameObject fx2 = (GameObject)Instantiate (Resources.Load("FX/Sky_5Star_Skill_FX"), transform.position, transform.rotation);
		fx2.transform.parent = transform;
		player.currentState = PlayerMovement.State.Attack;
		StartCoroutine (ParticleCleaner (5.0f, fx));
	}

	void Skill16(PlayerMovement player)
	{
		transform = player.transform;
		GameObject fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation) ;
		StartCoroutine (ParticleCleaner (5.0f, fx));
		
	}


	void Skill17(PlayerMovement player)
	{
		transform = player.transform;
		GameObject fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation) ;
		GameObject fx2 = (GameObject)Instantiate (Resources.Load("FX/Sky_5Star_Skill_FX"), transform.position, transform.rotation);
		fx2.transform.parent = transform;
		player.currentState = PlayerMovement.State.Attack;
		SphereCollider sphere = player.GetComponent<SphereCollider> ();
		AttackArea.ChangeAttackAreaTo (sphere, AttackArea.AreaType.Around);
		StartCoroutine (ParticleCleaner (5.0f, fx));
		
	}

	void Skill18(PlayerMovement player)
	{
		transform = player.transform;
		GameObject fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation) ;
		StartCoroutine (ParticleCleaner (5.0f, fx));
		
	}

	void Skill19(PlayerMovement player){
		
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		fx.transform.parent = transform;
		
		player.EatItem4 ();
		StartCoroutine (ParticleCleaner (10.0f, fx));
	}

	void Skill20(PlayerMovement player){
		
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		fx.transform.parent = transform;
		StartCoroutine (Skill20_Post (0.3f, player));
	}

	IEnumerator Skill20_Post(float waitTime, PlayerMovement obj){
		yield return new WaitForSeconds(waitTime);
		obj.currentState = PlayerMovement.State.Attack;
		Rigidbody playerRigidbody = obj.GetComponent <Rigidbody> ();
		playerRigidbody.AddForce (obj.transform.forward * 300, ForceMode.Impulse);
		StartCoroutine (ParticleCleaner (1.7f, fx));
	}

	void Skill21(PlayerMovement player){
		
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		fx.transform.parent = transform;
		player.currentState = PlayerMovement.State.Attack;
		SphereCollider sphere = player.GetComponent<SphereCollider> ();
		AttackArea.ChangeAttackAreaTo (sphere, AttackArea.AreaType.Around);
		StartCoroutine (ParticleCleaner (5.0f, fx));
		StartCoroutine (Skill21_Post (5.0f, player));
	}

	IEnumerator Skill21_Post(float waitTime, PlayerMovement obj){
		
		yield return new WaitForSeconds(waitTime);
		obj.currentState = PlayerMovement.State.Idle;
		SphereCollider sphere = obj.GetComponent<SphereCollider> ();
		AttackArea.ChangeAttackAreaTo (sphere, AttackArea.AreaType.Single);
	}

	void Skill22(PlayerMovement player){
		
		Skill19 (player);
	}

	void Skill23(PlayerMovement player)
	{
		Skill16 (player);
		
	}

	void Skill24(PlayerMovement player)
	{
		Skill08 (player);
		
	}

	void Skill25(PlayerMovement player)
	{
		Skill08 (player);
		
	}

	void Skill27(PlayerMovement player){
		
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		fx.transform.parent = transform;
		
		player.EatItem7 ();
		StartCoroutine (ParticleCleaner (10.0f, fx));
	}

	void Skill28(PlayerMovement player){
		
		Skill07 (player);
	}

	void Skill29(PlayerMovement player){
		
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		fx.transform.parent = transform;
		player.currentState = PlayerMovement.State.Attack;
		SphereCollider sphere = player.GetComponent<SphereCollider> ();
		AttackArea.ChangeAttackAreaTo (sphere, AttackArea.AreaType.Around);
		StartCoroutine (ParticleCleaner (5.0f, fx));
		StartCoroutine (Skill29_Post (5.0f, player));
	}
	
	IEnumerator Skill29_Post(float waitTime, PlayerMovement obj){
		
		yield return new WaitForSeconds(waitTime);
		obj.currentState = PlayerMovement.State.Idle;
		SphereCollider sphere = obj.GetComponent<SphereCollider> ();
		AttackArea.ChangeAttackAreaTo (sphere, AttackArea.AreaType.Single);
	}

	void Skill30(PlayerMovement player){
		
		Skill07 (player);
	}

	void Skill31(PlayerMovement player){
		
		Skill03 (player);
	}

	void Skill32(PlayerMovement player){
		
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		fx.transform.parent = transform;
		StartCoroutine (Skill32_Post (0.3f, player));
	}
	
	IEnumerator Skill32_Post(float waitTime, PlayerMovement obj){
		yield return new WaitForSeconds(waitTime);
		obj.currentState = PlayerMovement.State.Attack;
		Rigidbody playerRigidbody = obj.GetComponent <Rigidbody> ();
		playerRigidbody.AddForce (obj.transform.forward * 300, ForceMode.Impulse);
		StartCoroutine (ParticleCleaner (1.7f, fx));
	}

	void Skill33(PlayerMovement player){
		
		Skill16 (player);
	}

	void Skill34(PlayerMovement player){
		
		Skill07 (player);
	}

	void Skill35(PlayerMovement player){
		
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		fx.transform.parent = transform;
		StartCoroutine (Skill35_Post (0.3f, player));
	}
	
	IEnumerator Skill35_Post(float waitTime, PlayerMovement obj){
		yield return new WaitForSeconds(waitTime);
		obj.currentState = PlayerMovement.State.Attack;
		Rigidbody playerRigidbody = obj.GetComponent <Rigidbody> ();
		playerRigidbody.AddForce (obj.transform.forward * 300, ForceMode.Impulse);
		StartCoroutine (ParticleCleaner (1.7f, fx));
	}

	void Skill36(PlayerMovement player){
		
		Skill19 (player);
	}

	void Skill37(PlayerMovement player){
		
		Skill06 (player);
	}

	void Skill38(PlayerMovement player){
		
		Skill03 (player);
	}

	void Skill39(PlayerMovement player){
		
		Skill11 (player);
	}

	void Skill40(PlayerMovement player){
		
		Skill07 (player);
	}

	void Skill41(PlayerMovement player){
		
		Skill10 (player);
	}

	void Skill42(PlayerMovement player){
		
		Skill27 (player);
	}

	void Skill43(PlayerMovement player){
		
		Skill10 (player);
	}

	void ArrowRain(PlayerMovement player){
		player.currentState = PlayerMovement.State.Attack;
		transform = player.transform;
		fx = (GameObject)Instantiate (player.skill_fx, transform.position, transform.rotation);
		StartCoroutine (ParticleCleaner (5.0f, fx));
	}
	
	IEnumerator ParticleCleaner(float waitTime, GameObject obj){
		yield return new WaitForSeconds(waitTime);
		Destroy (obj);
	}
}
