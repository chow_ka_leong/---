﻿using UnityEngine;
using System.Collections;

public class skill003 : MonoBehaviour {

	void OnParticleCollision(GameObject other) {
		print ("collide_p");
		Rigidbody body = other.GetComponent<Rigidbody>();
		if (body) {
			Vector3 direction = other.transform.position - transform.position;
			direction = direction.normalized;
			body.AddForce(direction * 5);
		}
	}

	void OnTriggerStay(Collider col)
	{
		print ("collide");
	}
}
