﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {

	enum State {Idle ,PreAttack, Hit, Attack, Die};

	enum EnemyType {Hand , Sword, Rod, Bow};

	public int type_id;

	public GameObject weapon;

	GameObject equipment;

	State currentState;

	Transform playerInfo;

	NavMeshAgent nav;

	Animation anim; 

	private Transform transform;

	EnemyType type;

	public float hp = 0;

	float maxHP;

	public int attack = 0;

	float distance;

	void OnGUI()
	{

		if(Time.timeScale > 0){
			Vector2 targetPos;
			targetPos = Camera.main.WorldToScreenPoint (transform.position);
			
			GUI.DrawTexture (new Rect(targetPos.x-35/2, Screen.height- targetPos.y -55, 35 , 5) , Resources.Load("BattleUI/emeny_blood_barBG") as Texture );
			if(hp > 0)
				GUI.DrawTexture (new Rect(targetPos.x-35/2, Screen.height- targetPos.y -55, 35 * hp/maxHP, 5) , Resources.Load("BattleUI/emeny_blood_bar1") as Texture );
			
			

		}
		distance = (EnemyType.Bow == type) ? 5.0f : 1.7f;

		//GUI.Box(new Rect(targetPos.x, Screen.height- targetPos.y, 60, 20), hp + "/" + maxHP);
		
	}

	// Use this for initialization
	void Start () {

		maxHP = hp;

		playerInfo = GameObject.FindGameObjectWithTag ("Player").transform;
		nav = GetComponent<NavMeshAgent>();

		currentState = State.Idle;

		anim = GetComponent <Animation> ();

		transform = GetComponent <Transform> ();

		type = (EnemyType)type_id;

		if(weapon != null)
		{
			Equip();
		}

	}
	
	// Update is called once per frame
	void Update () {

		if(currentState == State.Attack || currentState == State.Die || currentState == State.PreAttack || currentState == State.Hit)
		{
			nav.enabled = false;
			return;
		}
			

		playerInfo = GameObject.FindGameObjectWithTag ("Player").transform;
		Chasing (playerInfo);
		if(Vector3.Distance(playerInfo.position, transform.position) < distance)
		{
			print ("Close enough");
			int random = Random.Range(0, 60);
			if(random == 1)
			{
				switch(type){
					
				case EnemyType.Hand:
					currentState = State.PreAttack;
					anim.Play("Attack_Hand_ENE");
					StartCoroutine(Attacking(anim["Attack_Hand_ENE"].length/2));
					break;
				case EnemyType.Sword:
					currentState = State.PreAttack;
					anim.Play("Attack_Sword_ENE");
					StartCoroutine(Attacking(anim["Attack_Sword_ENE"].length/2));
					break;
				case EnemyType.Rod:
					currentState = State.PreAttack;
					anim.Play("Attack_Spear_ENE");
					StartCoroutine(Attacking(anim["Attack_Spear_ENE"].length/2));
					break;
				case EnemyType.Bow:
					currentState = State.PreAttack;
					anim.Play("Attack_Bow_ENE");
					StartCoroutine(Shooting(anim["Attack_Bow_ENE"].length/2));
					break;
				}

				transform.rotation = Quaternion.LookRotation(playerInfo.position - transform.position);
			}
			else
			{
				Idle();
			}
		}
		else
		{
			Chasing(playerInfo);
		}


	}

	void Chasing(Transform playerInfo)
	{
		currentState = State.Idle;
		anim.CrossFade ("Run");
		nav.enabled = true;
		nav.SetDestination (playerInfo.position);
	}

	void Idle()
	{
		currentState = State.Idle;
		anim.CrossFade ("Idle1h_ENE");
		nav.enabled = false;
	}

	IEnumerator Shooting(float waitTime){
		yield return new WaitForSeconds(waitTime);
		if(currentState != State.Die)
		{
			//transform.rotation = Quaternion.LookRotation(playerInfo.position - transform.position);
			GameObject arrow = (GameObject)Instantiate(Resources.Load("Prefabs/Arrow_ENE"), transform.position + transform.up * 1, transform.rotation);
			Arrow_ENE info = arrow.GetComponent<Arrow_ENE>();
			info.setATK(attack * Random.Range(0.7f,1.2f));
			arrow.GetComponent<Rigidbody>().AddForce(transform.forward*300.0f);
			currentState = State.Attack;
			StartCoroutine(Finishing(waitTime/2));
		}
		
	}

	IEnumerator Attacking(float waitTime){
		yield return new WaitForSeconds(waitTime);
		if(currentState != State.Die)
		{
			currentState = State.Attack;
			StartCoroutine(Finishing(waitTime/2));
		}

	}

	IEnumerator Finishing(float waitTime){
		yield return new WaitForSeconds(waitTime);
		currentState = State.Idle;
	}

	void Equip(){

		if(!weapon)
			return;

		Transform right_hand =  transform.FindChild ("Bip001").FindChild ("Bip001 Pelvis").FindChild ("Bip001 Spine").FindChild ("Bip001 Spine1").FindChild ("Bip001 Neck").FindChild ("Bip001 R Clavicle").FindChild ("Bip001 R UpperArm").FindChild ("Bip001 R Forearm").FindChild ("Bip001 R Hand").FindChild ("Bip001_Weapon Point R");
		if (!right_hand)
			right_hand = transform.FindChild ("Bip001").FindChild ("Bip001 Pelvis").FindChild ("Bip001 Spine").FindChild ("Bip001 Spine1").FindChild ("Bip001 Neck").FindChild ("Bip001 R Clavicle").FindChild ("Bip001 R UpperArm").FindChild ("Bip001 R Forearm").FindChild ("Bip001 R Hand").FindChild ("Bip001_Weapon Point");
		equipment = Instantiate(weapon, transform.position, transform.rotation) as GameObject;	

		if(type_id == 3)
		{
			right_hand = transform.FindChild ("Bip001").FindChild ("Bip001 Pelvis").FindChild ("Bip001 Spine").FindChild ("Bip001 Spine1").FindChild ("Bip001 Neck").FindChild ("Bip001 L Clavicle").FindChild ("Bip001 L UpperArm").FindChild ("Bip001 L Forearm").FindChild ("Bip001 L Hand").FindChild ("Bip001_Weapon Point L");
			if(!right_hand)
				right_hand = transform.FindChild ("Bip001").FindChild ("Bip001 Pelvis").FindChild ("Bip001 Spine").FindChild ("Bip001 Spine1").FindChild ("Bip001 Neck").FindChild ("Bip001 L Clavicle").FindChild ("Bip001 L UpperArm").FindChild ("Bip001 L Forearm").FindChild ("Bip001 L Hand").FindChild ("Bip001_Weapon Point");

		}

		equipment.transform.position = right_hand.position;
		equipment.transform.rotation = right_hand.rotation;

		equipment.transform.parent = right_hand;

		equipment.gameObject.tag = "Weapon_ENE";

	}

	public string getStatus()
	{
		return currentState.ToString ();
	}

	public string getHealth()
	{
		return currentState.ToString ();
	}

	public int getAttack()
	{
		return attack;
	}

	public void hit(int val)
	{

		hp -= val;

		if(hp <= 0)
		{
			//Die
			nav.enabled = false;
			Destroy(GetComponent<Rigidbody>());
			Destroy(GetComponent<CapsuleCollider>());
			Destroy(equipment);
			currentState = State.Die;
			anim.Stop();
			anim.Play("Die");
			Instantiate(Resources.Load("FX/Hitted_FX"),transform.position,transform.rotation);
			StartCoroutine(Cleaup(1.0f));

		}
		else
		{

//			Instantiate(Resources.Load("FX/Hitted_FX"),transform.position,transform.rotation);

//			if(currentState == State.Attack || currentState == State.PreAttack)
//				return;

			//Hit
			currentState = State.Hit;
//			anim.Play("Hitted");
			StartCoroutine(ResetState(0.5f));
		}

	}

	IEnumerator ResetState(float waitTime){
		yield return new WaitForSeconds(waitTime);
		currentState = State.Idle;
	}

	IEnumerator Cleaup(float waitTime){
		yield return new WaitForSeconds(waitTime);
		Destroy (transform.root.gameObject);
	}
}
