﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Global : MonoBehaviour {

	public static int chapter = 1;
	public static int level = 1;


	void Update(){
		if (Input.deviceOrientation == DeviceOrientation.LandscapeRight)
		{
			Screen.orientation = ScreenOrientation.LandscapeRight;
		}
		else if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
		{
			Screen.orientation = ScreenOrientation.LandscapeLeft;
		}
	}

	public static string player_1_id
	{
		get
		{
			return PlayerPrefs.GetString("Player1");
		}
		set
		{
			PlayerPrefs.SetString("Player1", value);
		}
	}
	public static string player_2_id
	{
		get
		{
			return PlayerPrefs.GetString("Player2");
		}
		set
		{
			PlayerPrefs.SetString("Player2", value);
		}
	}

	private string[] player_list
	{
		get
		{
			return PlayerPrefsX.GetStringArray("PlayerList");
		}
	}

	Vector3 visible = new Vector3(1,1,1);
	Vector3 invisible = new Vector3(0,0,0);


	public void StartGame()
	{
		Application.LoadLevel("SelectChapter");
	}

	public void backToMain()
	{
		Application.LoadLevel("MainUI");
	}

	public void selectChapter(int index)
	{

		chapter = index;

	}

	public void selectLevel (int index)
	{

		level = index;

	}

	public void backToChapter()
	{

		GameObject level_go = GameObject.Find ("Level") as GameObject;
		GameObject chapter_go = GameObject.Find ("Chapter") as GameObject;

		level_go.transform.localScale = invisible;
		chapter_go.transform.localScale = visible;

	}

	public void StartLevel()
	{

		Application.LoadLevel("Level"+chapter.ToString()+"-"+level.ToString());
	}

	public void selectTeam()
	{
		Application.LoadLevel("SelectTeam");
	}

	public string getPlayerID(int index)
	{
		if(index == 0)
			return player_1_id;
		else
			return player_2_id;
	}

	public string[] getPlayerList()
	{
		return player_list;
	}

	public void switch_main_sub()
	{
		string tmp = player_1_id;

		player_1_id = player_2_id;
		player_2_id = tmp;
	}

	public void setPlayer1(string ID)
	{
		player_1_id = ID;
	}

	public void setPlayer2(string ID)
	{
		player_2_id = ID;
	}

	public static string getLevel()
	{
		return chapter +"-"+ level;
	}


}
