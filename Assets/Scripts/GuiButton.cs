using UnityEngine;
using System.Collections;

[AddComponentMenu("gui control/GuiButton")]
public class GuiButton : MonoBehaviour
{
	public const int BASE_GUI_WIDTH = 960;
	public const int BASE_GUI_HEIGHT = 640;
	
	// resource
	public Texture2D upTexture;
	public Texture2D downTexture;
	public Color enableColor = Color.white;
	public Texture2D disableTexture;
	public Color disableColor = Color.grey;
	public Rect buttonRect;
	public TextAnchor	rectAnchor; // TextAnchor.LowerLeft
	
	// internal component
	public GUITexture	guiTextureComponent;
	
	// setting
	public string inputAxisName; // = "Horizontal"
	
	const int FINGERID_NONE = -1;
	const int FINGERID_MOUSE = -2;
	const int FINGERID_INPUTAXIS = -3;
	protected int lastFingerId = FINGERID_NONE;
	
	
	bool	buttonDown;
	bool	buttonDownChange;
	bool	buttonUpInRect;
	float buttonDownStartTime; // realtime
	int	buttonDownFrame;
	bool	toggleButtonDown;
	bool	swapUpDownTexture;
	bool	ignoreInput;
	
	public delegate void ButtonDownChangeDelegate(GuiButton button);
	public event ButtonDownChangeDelegate onButtonEvent;
	public object buttonCustomId;
	
	public bool SwapUpDownTexture
	{
		get { return swapUpDownTexture; }
		set
		{
			if (swapUpDownTexture != value)
			{
				swapUpDownTexture = value;
				guiTextureComponent.texture = (buttonDown ^ swapUpDownTexture) ? downTexture : upTexture;
			}
		}
	}
	public Texture2D UpTexture
	{
		get { return upTexture; }
		set
		{
			upTexture = value;
			if (enabled && !(buttonDown ^ swapUpDownTexture))
				guiTextureComponent.texture = upTexture;
		}
	}
	public Texture2D DownTexture
	{
		get { return downTexture; }
		set
		{
			downTexture = value;
			if (enabled && (buttonDown ^ swapUpDownTexture))
				guiTextureComponent.texture = downTexture;
		}
	}
	public Texture2D DisableTexture
	{
		get { return disableTexture; }
		set
		{
			disableTexture = value;
			if (!enabled)
				guiTextureComponent.texture = disableTexture;
		}
	}
	public bool ToggleButtonDown   // every GetButtonDown will toggle the state
	{
		get { return toggleButtonDown; }
		set { toggleButtonDown = value; }
	}
	
	public Color EnableColor
	{
		get { return enableColor; }
		set
		{
			enableColor = value;
			if (enabled)
				guiTextureComponent.color = enableColor * 0.5f;
		}
	}
	public Color DisableColor
	{
		get { return disableColor; }
		set
		{
			disableColor = value;
			if (!enabled)
				guiTextureComponent.color = disableColor * 0.5f;
		}
	}
	
	
	void Awake()
	{
		if (guiTextureComponent == null)
		{
			guiTextureComponent = GetComponent<GUITexture>();
		}
		buttonDown = false;
		buttonDownChange = false;
		buttonUpInRect = false;
		toggleButtonDown = false;
		lastFingerId = FINGERID_NONE;
		useGUILayout = false;
	}
	
	// to make invsible, make gameObject.active = false
	// to make disable state, this component enabled = false
	
	void OnEnable()
	{
		buttonDown = false;
		buttonDownChange = false;
		buttonUpInRect = false;
		lastFingerId = FINGERID_NONE;
	}
	
	void OnDisable()
	{
		buttonDown = false;
		buttonDownChange = false;
		buttonUpInRect = false;
		lastFingerId = FINGERID_NONE;
	}
	
	// instead of enabled = ?, so can set texture and color properly
	public void SetEnable( bool enableFlag )
	{
		enabled = enableFlag;
		
		if (enableFlag)
		{
			guiTextureComponent.texture = (buttonDown ^ swapUpDownTexture) ? downTexture : upTexture;
			guiTextureComponent.color = enableColor * 0.5f;
		}
		else
		{
			guiTextureComponent.texture = disableTexture;
			guiTextureComponent.color = disableColor * 0.5f;
		}
	}
	
	public void SetInputEnable( bool enableInputFlag )
	{
		ignoreInput = !enableInputFlag;
	}
	
	public bool GetInputEnable()
	{
		return !ignoreInput;
	}

	// Update is called once per frame
	void Update()
	{
		int newFingerId = FINGERID_NONE;
		bool releaseInRect = false;

		if (!ignoreInput)
		{
			Vector2 scaleVector = getGUIScale();
			Rect scaledDetectRect = transformRect( buttonRect, scaleVector, rectAnchor );
			
			if (Input.touchCount > 0)
			{
				// not using getGUIRect, but ScaleRect
				for(int t = 0; t < Input.touchCount; ++t)
				{
					Touch touch = Input.GetTouch(t);
					Vector2 yDownPos = new Vector2( touch.position.x, Screen.height - touch.position.y );
					// assume y down
					if (touch.phase == TouchPhase.Began && scaledDetectRect.Contains(yDownPos)
						|| (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary) && lastFingerId == touch.fingerId )
					{
						if (newFingerId == FINGERID_NONE || touch.fingerId == lastFingerId) // bias more to lastFingerId
						{
							newFingerId = touch.fingerId;
						}
					}
					else if ( (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) && lastFingerId == touch.fingerId)
					{
						releaseInRect = scaledDetectRect.Contains(yDownPos);
						newFingerId = FINGERID_NONE;
						break;
					}
				}
			}
			
			if (Input.touchCount == 0 && Input.GetMouseButton(0))
			{
				// not using getGUIRect, but ScaleRect
				// assume Y up/down?
				Vector2 yDownPos = new Vector2( Input.mousePosition.x, Screen.height - Input.mousePosition.y );
				if (Input.GetMouseButtonDown(0) && scaledDetectRect.Contains(yDownPos)
					|| FINGERID_MOUSE == lastFingerId)
				{
					if (newFingerId == FINGERID_NONE || FINGERID_MOUSE == lastFingerId) // bias more to lastFingerId
					{
						newFingerId = FINGERID_MOUSE;
					}
				}
			}
			else if (Input.GetMouseButtonUp(0) && lastFingerId == FINGERID_MOUSE)
			{
				Vector2 yDownPos = new Vector2( Input.mousePosition.x, Screen.height - Input.mousePosition.y );
				releaseInRect = scaledDetectRect.Contains(yDownPos);
				newFingerId = FINGERID_NONE;
			}

			bool inputAxisValue = string.IsNullOrEmpty(inputAxisName) ? false : Input.GetButton(inputAxisName);
			if (inputAxisValue)
			{
				if (newFingerId == FINGERID_NONE || FINGERID_INPUTAXIS == lastFingerId) // bias more to lastFingerId
				{
					newFingerId = FINGERID_INPUTAXIS;
				}
			}
			else if (lastFingerId == FINGERID_INPUTAXIS)
			{
				releaseInRect = true;
				newFingerId = FINGERID_NONE;
			}
		}

		bool oldButtonDown = buttonDown;
		buttonDown = (newFingerId != FINGERID_NONE);
		buttonDownChange = (oldButtonDown != buttonDown);
		lastFingerId = newFingerId;
		if (buttonDownChange)
		{
			guiTextureComponent.texture = (buttonDown ^ swapUpDownTexture) ? downTexture : upTexture;



			if (buttonDown)
			{
				buttonDownStartTime = Time.realtimeSinceStartup;
				buttonDownFrame = 0;
				toggleButtonDown = !toggleButtonDown;
			}
			else
			{
				buttonUpInRect = releaseInRect;
			}
		}
		else if (buttonDown)
		{
			++buttonDownFrame;
		}

		if (buttonDownChange && !ignoreInput && onButtonEvent != null)
		{
			onButtonEvent(this);
		}
	}
	
	//~ void OnGUI()
	//~ {
		//~ if (Event.current.type == EventType.Repaint)
		//~ {
			//~ Vector2 scaleVector = getGUIScale();
			//~ Color backupColor = GUI.color;
			
			//~ GUI.color = enabled ? enableColor : disableColor;
			//~ GUI.DrawTexture( transformRect( buttonRect, scaleVector, rectAnchor ), 
				//~ enabled ? (buttonDown ^ swapUpDownTexture ? downTexture : upTexture) : disableTexture );
			//~ GUI.color = backupColor;
		//~ }
	//~ }
	
	public bool GetButtonDown()
	{
		return buttonDownChange && buttonDown;
	}
	public bool GetButton()
	{
		return buttonDown;
	}
	public bool GetButtonUp() // not including  buttonDown = false by disable
	{
		return buttonDownChange && !buttonDown;
	}
	public bool GetButtonPress() // press then release quickly
	{
		return buttonDownChange && !buttonDown && buttonUpInRect && (Time.realtimeSinceStartup - buttonDownStartTime <= 0.5f || buttonDownFrame <= 4);
	}
	
	public static Rect getGUIRect()
	{
		return adjustViewPort.getGUIRect( BASE_GUI_WIDTH, BASE_GUI_HEIGHT );
	}
	public static Rect ScaleRect( Rect srcRect, Rect multiRect )
	{
		return new Rect( srcRect.x * multiRect.width / BASE_GUI_WIDTH, srcRect.y * multiRect.height / BASE_GUI_HEIGHT, srcRect.width * multiRect.width / BASE_GUI_WIDTH, srcRect.height * multiRect.height / BASE_GUI_HEIGHT );
	}
	public static void anchorToOffset( TextAnchor anchor, out int baseX, out int baseY )
	{
		switch( anchor )
		{
		case TextAnchor.UpperLeft:
			baseY = 0;
			baseX = 0;
			break;
		case TextAnchor.UpperCenter:
			baseY = 0;
			baseX = Screen.width / 2;
			break;
		case TextAnchor.UpperRight:
			baseY = 0;
			baseX = Screen.width;
			break;
		case TextAnchor.MiddleLeft:
			baseY = Screen.height / 2;
			baseX = 0;
			break;
		case TextAnchor.MiddleCenter:
			baseY = Screen.height / 2;
			baseX = Screen.width / 2;
			break;
		case TextAnchor.MiddleRight:
			baseY = Screen.height / 2;
			baseX = Screen.width;
			break;
		case TextAnchor.LowerLeft:
			baseY = Screen.height;
			baseX = 0;
			break;
		case TextAnchor.LowerCenter:
			baseY = Screen.height;
			baseX = Screen.width / 2;
			break;
		case TextAnchor.LowerRight:
			baseY = Screen.height;
			baseX = Screen.width;
			break;
		default:
			baseY = 0;
			baseX = 0;
			break;
		}
	}
	public static Vector2 getGUIScale()
	{
		Rect guiRect = adjustViewPort.getGUIRect( BASE_GUI_WIDTH, BASE_GUI_HEIGHT );
		return new Vector2( guiRect.width / BASE_GUI_WIDTH, guiRect.height / BASE_GUI_HEIGHT );
	}
	public static Vector2 transformPoint( Vector2 unscalePos, Vector2 scaleVector, TextAnchor anchor ) // not within BeginGroup( getGUIRect() )
	{
		int baseX;
		int baseY;
		anchorToOffset( anchor, out baseX, out baseY );
		return new Vector2( baseX+unscalePos.x*scaleVector.x, baseY+unscalePos.y*scaleVector.y );
	}
	public static Vector2 invTransformPoint( Vector2 scalePos, Vector2 scaleVector, TextAnchor anchor ) // not within BeginGroup( getGUIRect() )
	{
		int baseX;
		int baseY;
		anchorToOffset( anchor, out baseX, out baseY );
		return new Vector2( (scalePos.x-baseX)/scaleVector.x, (scalePos.y-baseY)/scaleVector.y );
	}
	public static Rect transformRect( Rect unscaleRect, Vector2 scaleVector, TextAnchor anchor ) // not within BeginGroup( getGUIRect() )
	{
		int baseX;
		int baseY;
		anchorToOffset( anchor, out baseX, out baseY );
		return new Rect( baseX+unscaleRect.x*scaleVector.x,
			baseY+unscaleRect.y*scaleVector.y,
			unscaleRect.width*scaleVector.x,
			unscaleRect.height*scaleVector.y );
	}
	
	public static void anchorToScreenSpace( TextAnchor anchor, out float baseX, out float baseY )
	{
		switch( anchor )
		{
		case TextAnchor.UpperLeft:
			baseY = 1.0f;
			baseX = 0.0f;
			break;
		case TextAnchor.UpperCenter:
			baseY = 1.0f;
			baseX = 0.5f;
			break;
		case TextAnchor.UpperRight:
			baseY = 1.0f;
			baseX = 1.0f;
			break;
		case TextAnchor.MiddleLeft:
			baseY = 0.5f;
			baseX = 0.0f;
			break;
		case TextAnchor.MiddleCenter:
			baseY = 0.5f;
			baseX = 0.5f;
			break;
		case TextAnchor.MiddleRight:
			baseY = 0.5f;
			baseX = 1.0f;
			break;
		case TextAnchor.LowerLeft:
			baseY = 0.0f;
			baseX = 0.0f;
			break;
		case TextAnchor.LowerCenter:
			baseY = 0.0f;
			baseX = 0.5f;
			break;
		case TextAnchor.LowerRight:
			baseY = 0.0f;
			baseX = 1.0f;
			break;
		default:
			baseY = 0.0f;
			baseX = 0.0f;
			break;
		}
	}
	public static Rect ScaleRectToPixelInsert( Rect srcRect, Vector2 scaleVector )
	{
		return new Rect( srcRect.x * scaleVector.x, -srcRect.y * scaleVector.y, srcRect.width * scaleVector.x, srcRect.height * scaleVector.y );
	}
	
	[ContextMenu("Fill buttonRect")]
	public void FillButtonRect()
	{
		float centerX = transform.position.x;
		float centerY = transform.position.y;
		float sizeX = transform.localScale.x;
		float sizeY = transform.localScale.y;
		float anchorBaseX;
		float anchorBaseY;
		anchorToScreenSpace( rectAnchor, out anchorBaseX, out anchorBaseY );
		
		for( Transform parentTrans = transform.parent; (parentTrans != null); parentTrans = parentTrans.parent)
		{
			sizeX *= parentTrans.localScale.x;
			sizeY *= parentTrans.localScale.y;
		}

		buttonRect = new Rect( 
			Mathf.Round((centerX - anchorBaseX - sizeX * 0.5f) * BASE_GUI_WIDTH),
			Mathf.Round((anchorBaseY - centerY - sizeY * 0.5f) * BASE_GUI_HEIGHT),
			Mathf.Round(sizeX * BASE_GUI_WIDTH),
			Mathf.Round(sizeY * BASE_GUI_HEIGHT) );
	}
	
	
}
