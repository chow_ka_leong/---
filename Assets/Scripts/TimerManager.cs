﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimerManager : MonoBehaviour {

	public GameObject timer_1;
	public GameObject timer_2;
	public GameObject timer_3;
	public GameObject timer_4;
	public GameObject timer_5;
	public GameObject timer_6;

	public float timeLeft;

	public Texture[] img;

	int min;
	int sec;
	int hour;

	// Use this for initialization
	void Start () {


		updateTimer ();


	}

	void Update () {
		timeLeft -= Time.deltaTime;
		if ( timeLeft < 0 )
		{
			//Time's Up
		}
		else
		{
			updateTimer();
		}
	}

	void updateTimer(){

		hour = (int)(timeLeft / 60 / 60);
		min = (int)(timeLeft / 60 );
		sec = (int)(timeLeft % 60 );

		if(min >= 10){
			timer_3.GetComponent<GUITexture>().texture = img[(int)(min/10)];
			timer_4.GetComponent<GUITexture>().texture = img[(int)(min%10)];
		}
		else{
			timer_3.GetComponent<GUITexture>().texture = img[0];
			timer_4.GetComponent<GUITexture>().texture = img[(int)(min%10)];
		}
		if(sec >= 10){
			timer_5.GetComponent<GUITexture>().texture = img[(int)(sec/10)];
			timer_6.GetComponent<GUITexture>().texture = img[(int)(sec%10)];
		}
		else{
			timer_5.GetComponent<GUITexture>().texture = img[0];
			timer_6.GetComponent<GUITexture>().texture = img[(int)(sec%10)];
		}

		if(hour >= 10){
			timer_1.GetComponent<GUITexture>().texture = img[(int)(hour/10)];
			timer_2.GetComponent<GUITexture>().texture = img[(int)(hour%10)];
		}
		else{
			timer_1.GetComponent<GUITexture>().texture = img[0];
			timer_2.GetComponent<GUITexture>().texture = img[(int)(hour%10)];
		}
	}
}
