﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthHUD : MonoBehaviour {

	public PlayerManager player;
	public Texture hp;
	public Texture hp_bg;

	public Texture charTexture;

	//the GUI scale ratio  
	private float guiRatio;  
	
	//the screen width  
	private float sWidth;  
	
	//create a scale Vector3 with the above ratio  
	private Vector3 GUIsF; 

	float ratio = (float)Screen.height / (float)Screen.width;

	void Awake()  
	{  
		//get the screen's width  
		sWidth = Screen.width;  
		//calculate the scale ratio  
		guiRatio = (float)Screen.height/640.0f;  
		//create a scale Vector3 with the above ratio  
		GUIsF = new Vector3(guiRatio,guiRatio,1);  


	}  

	void OnGUI(){
		GUI.matrix = Matrix4x4.TRS(Vector3.zero,Quaternion.identity,GUIsF); 

		GUI.DrawTexture (new Rect (30, 30, 125, 129) , charTexture );

		GUI.DrawTexture (new Rect (30, 30, 125, 129) , Resources.Load("TeamUI/CHAR_ICON/CHAR_ICON_SKY0" + PlayerManager.playerInActive) as Texture );

		GUI.matrix = Matrix4x4.TRS(new Vector3(155*GUIsF.x,50*GUIsF.y ,0),Quaternion.identity,GUIsF); 

		Rect rect = new Rect (10, 10, (260*player.getHealth()/100.0f), 28);
		Rect rect2 = new Rect (0, 0, 284, 53);

		GUI.DrawTexture (rect2 , hp_bg );
		GUI.DrawTexture (rect , hp );

	}
	
}
