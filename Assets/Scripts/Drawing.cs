﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Drawing : MonoBehaviour {

	private string draw_type;
	public GameObject resultPanel;

	public Button btn_diamond;
	public Button btn_money;

	public Image character;

	void Start () {
		//AddPlayer ("38");
	}
	
	// Update is called once per frame
	void Update () {
		if( PlayerPrefs.GetInt ("money") < 3000){

			//btn_money.interactable = false;

		}
	}

	public void Closing(){
		gameObject.SetActive (false);
		resultPanel.SetActive (false);
	}

	public void Diamond_Drawing(){
		draw_type = "diamond";

		if(PlayerPrefs.GetInt ("diamond") < 15){

			MainUI main = GameObject.Find ("SceneManager").GetComponent<MainUI>();
			main.Shop();

			gameObject.SetActive(false);

		}
		else{
			//drawing
			int d = PlayerPrefs.GetInt ("diamond");
			PlayerPrefs.SetInt("diamond",d-15);
			PlayerPrefs.Save();

			int random = Random.Range(1,100);
			int rare = 0;

			if(random < 61){
				rare = 3;
			}
			else if(random < 91){
				rare = 4;
			}
			else{
				rare = 5;
			}

			string ID = "";

			bool isDone = false;

			while(!isDone){
				int rand = Random.Range(1,41);
				print (CharacterStat.getCharacterData(rand.ToString())["rare"]);
				int temp_rare = 0;
				if(int.TryParse(CharacterStat.getCharacterData(rand.ToString())["rare"], out temp_rare)){
					if(int.Parse(CharacterStat.getCharacterData(rand.ToString())["rare"]) == rare){

						ID = CharacterStat.getCharacterData(rand.ToString())["ID"].Replace("SKY0","");
						isDone = true;

					}
				}

			}

			character.sprite = Resources.Load<Sprite> ("TeamUI/CHAR_ICON/CHAR_ICON_SKY0" + ID);

			bool isExist = false;
			string[] playerList = PlayerPrefsX.GetStringArray("PlayerList");
			List<string> list = new List<string>();

			foreach(string str in playerList){
				list.Add(str);
				if(ID == str)
					isExist = true;

			}

			if(isExist){
				string[] data = PlayerPrefsX.GetStringArray("Char" + ID);
				if(int.Parse(data[0]) < 30)
					PlayerPrefsX.SetStringArray("Char" + ID, new string[]{(int.Parse(data[0]) + 1).ToString(), "0"});
			}
			else{
				PlayerPrefsX.SetStringArray("Char" + ID, new string[]{"1", "0"});
				list.Add(ID);
				list.Sort();
				PlayerPrefsX.SetStringArray("PlayerList", list.ToArray());
			}



			resultPanel.SetActive (true);
		}


	}

	public void Money_Drawing(){
		draw_type = "money";

		if( PlayerPrefs.GetInt ("money") < 3000){
			
			resultPanel.SetActive (false);
			MainUI main = GameObject.Find ("SceneManager").GetComponent<MainUI>();
			main.addmoney(1);

			gameObject.SetActive(false);
		}
		else{
			int d = PlayerPrefs.GetInt ("money");
			PlayerPrefs.SetInt("money",d-3000);
			PlayerPrefs.Save();
			
			int random = Random.Range(1,100);
			int rare = 0;
			
			if(random < 41){
				rare = 1;
			}
			else if(random < 91){
				rare = 2;
			}
			else{
				rare = 3;
			}
			
			string ID = "";
			
			bool isDone = false;
			
			while(!isDone){
				int rand = Random.Range(1,41);
				string rand_str = rand.ToString();

				if(rand < 10)
					rand_str = "0"+rand_str;
				if(int.Parse(CharacterStat.getCharacterData(rand_str)["rare"]) == rare){
					
					ID = CharacterStat.getCharacterData(rand_str)["ID"].Replace("SKY0","");
					isDone = true;
					
				}
				
			}
			
			character.sprite = Resources.Load<Sprite> ("TeamUI/CHAR_ICON/CHAR_ICON_SKY0" + ID);
			
			bool isExist = false;
			string[] playerList = PlayerPrefsX.GetStringArray("PlayerList");
			List<string> list = new List<string>();
			
			foreach(string str in playerList){
				list.Add(str);
				if(ID == str)
					isExist = true;
				
			}
			
			if(isExist){
				string[] data = PlayerPrefsX.GetStringArray("Char" + ID);
				if(int.Parse(data[0]) < 30)
					PlayerPrefsX.SetStringArray("Char" + ID, new string[]{(int.Parse(data[0]) + 1).ToString(), "0"});
			}
			else{
				PlayerPrefsX.SetStringArray("Char" + ID, new string[]{"1", "0"});
				list.Add(ID);
				list.Sort();
				PlayerPrefsX.SetStringArray("PlayerList", list.ToArray());
			}
			
			
			
			resultPanel.SetActive (true);
		}

		//resultPanel.SetActive (true);
	}

	public void DrawAgain(){
	
		if(draw_type == "diamond"){
			Diamond_Drawing();
		}
		else{
			Money_Drawing();
		}
	
	}

	public void closeResult(){
		resultPanel.SetActive (false);
	}

	void AddPlayer(string ID){

		bool isExist = false;
		string[] playerList = PlayerPrefsX.GetStringArray("PlayerList");
		List<string> list = new List<string>();
		
		foreach(string str in playerList){
			print (str);
			list.Add(str);
			if(ID == str)
				isExist = true;
			
		}
		
		if(isExist){
			string[] data = PlayerPrefsX.GetStringArray("Char" + ID);
			PlayerPrefsX.SetStringArray("Char" + ID, new string[]{(int.Parse(data[0]) + 1).ToString(), "0"});
		}
		else{
			PlayerPrefsX.SetStringArray("Char" + ID, new string[]{"1", "0"});
			list.Add(ID);
			list.Sort();
			PlayerPrefsX.SetStringArray("PlayerList", list.ToArray());
		}

	}
}
