using UnityEngine;
using System.Collections;

public class adjustViewPort : MonoBehaviour
{
	const int BASE_VIEWPORT_WIDTH = 960;
	const int BASE_VIEWPORT_HEIGHT = 640;

	public Texture2D fillTexture;
	private int lastScreenWidth = 0;
	private int lastScreenHeight = 0;
	private Rect lastGUIRect = new Rect( 0, 0, 1, 1 );

	static int bannerTopHeight = 0;
	
	void Awake()
	{
#if UNITY_ANDROID
		GameObject bannerGameObj = GameObject.Find( "AdMobAndroidBanner" );
		if (bannerGameObj != null && bannerGameObj.active)
		{
			// since AdMobAndroidAd.phone320x50
			bannerTopHeight = 0; // Mathf.FloorToInt( AdMobAndroid.getScreenDensity() * 50.0f );
		}
		else
		{
			bannerTopHeight = 0;
		}
#endif
		useGUILayout = false;
	}

	void OnPreCull()
	{
		// adjust view port
		if (lastScreenWidth != Screen.width || lastScreenHeight != Screen.height)
		{
			// asssume size is 2:3
			// height / width > 1.5  if not adjust viewport, will view to narrow (in screen x)
			if ((Screen.height-bannerTopHeight) * BASE_VIEWPORT_WIDTH >= Screen.width * BASE_VIEWPORT_HEIGHT)
			{
				int effHeight = Screen.width * BASE_VIEWPORT_HEIGHT / BASE_VIEWPORT_WIDTH;
				lastGUIRect = new Rect( 0, bannerTopHeight+(Screen.height-bannerTopHeight-effHeight)/2, Screen.width, effHeight );
				gameObject.GetComponent<Camera>().pixelRect = new Rect( 0, (Screen.height-effHeight)/2, Screen.width, effHeight );
				// seems no need to change gameObject.camera.aspect
			}
			// height / width < 1.4  if not adjust viewport, will view to wide (in screen y)
			else
			{
				int effWidth = (Screen.height-bannerTopHeight) * BASE_VIEWPORT_WIDTH / BASE_VIEWPORT_HEIGHT;
				lastGUIRect = new Rect( (Screen.width-effWidth)/2, bannerTopHeight, effWidth, (Screen.height-bannerTopHeight) );
				gameObject.GetComponent<Camera>().pixelRect = new Rect( (Screen.width-effWidth)/2, 0, effWidth, (Screen.height-bannerTopHeight) );
			}
			
			lastScreenWidth = Screen.width;
			lastScreenHeight = Screen.height;
		}
	}


	#if UNITY_ANDROID
	// blacken the unused area, need for android
	void OnGUI()
	{
		if (lastScreenWidth != 0 || lastScreenHeight != 0)
		{
			if (lastGUIRect.yMin > 0)
			{
				GUI.DrawTexture( Rect.MinMaxRect( 0, 0, Screen.width, lastGUIRect.yMin), fillTexture );
			}
			if (lastGUIRect.yMax < Screen.height)
			{
				GUI.DrawTexture( Rect.MinMaxRect( 0, lastGUIRect.yMax, Screen.width, Screen.height), fillTexture );
			}
			if (lastGUIRect.xMin > 0)
			{
				GUI.DrawTexture( Rect.MinMaxRect( 0, 0, lastGUIRect.xMin, Screen.height ), fillTexture );
			}
			if (lastGUIRect.xMax < Screen.width)
			{
				GUI.DrawTexture( Rect.MinMaxRect( lastGUIRect.xMax, 0, Screen.width, Screen.height), fillTexture );
			}
		}
	}
	#endif


	public static Rect getGUIRect( int baseWidth, int baseHeight )
	{
		if ((Screen.height-bannerTopHeight) * baseWidth >= Screen.width * baseHeight)
		{
			int effHeight = Screen.width * baseHeight / baseWidth;
			return new Rect( 0, bannerTopHeight+(Screen.height-bannerTopHeight-effHeight)/2, Screen.width, effHeight );
		}
		else
		{
			int effWidth = (Screen.height-bannerTopHeight)*baseWidth/baseHeight;
			return new Rect( (Screen.width-effWidth)/2, bannerTopHeight, effWidth, (Screen.height-bannerTopHeight) );
		}
	}

	// multiRect is from getGUIRect
	public static Rect ScaleRect( Rect srcRect, Rect multiRect, int baseWidth, int baseHeight ) 
	{
		return new Rect( srcRect.x * multiRect.width / baseWidth, srcRect.y * multiRect.height / baseHeight, srcRect.width * multiRect.width / baseWidth, srcRect.height * multiRect.height / baseHeight );
	}
}
