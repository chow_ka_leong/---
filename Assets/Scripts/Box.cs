﻿using UnityEngine;
using System.Collections;

public class Box : MonoBehaviour {

	enum State {Idle ,Break};

	State currentState;

	Animation anim;

	void Awake()
	{
		anim = GetComponent<Animation>();

		currentState = State.Idle;
	}


	public void Break()
	{
		if(currentState == State.Break)
			return;
		currentState = State.Break;
		Instantiate(Resources.Load("FX/WoodenBoxFX"),transform.position + transform.up * 1,transform.rotation);
		anim.Play ("Break");
		BoxCollider collider = GetComponentInChildren<BoxCollider> ();
		collider.enabled = false;
		ShowItem ();
		StartCoroutine(cleanup(2.0f));
	}

	IEnumerator cleanup(float sec)
	{
		yield return new WaitForSeconds(sec);
		Destroy (this.gameObject);
	}

	void ShowItem()
	{

		int random = Random.Range (1, 100);

		if(random <= 8)
		{
			//Nothing
		}
		else if(random <= 20)
		{
			//Heal 25%
			Instantiate (Resources.Load ("Prefabs/Item/Bread"), transform.position, transform.rotation);
		}
		else if(random <= 28)
		{
			//Heal 50%
			Instantiate (Resources.Load ("Prefabs/Item/Bread_Triple"), transform.position, transform.rotation);
		}
		else if(random <= 33)
		{
			//Heal 100%
			Instantiate (Resources.Load ("Prefabs/Item/FruitPlate"), transform.position, transform.rotation);
		}
		else if(random <= 41)
		{
			//Attack 20%
			Instantiate (Resources.Load ("Prefabs/Item/LittleSword"), transform.position, transform.rotation);
		}
		else if(random <= 49)
		{
			//Evasion Speed 20%
			Instantiate (Resources.Load ("Prefabs/Item/Shoe"), transform.position, transform.rotation);
		}
		else if(random <= 57)
		{
			//Critical 20%
			Instantiate (Resources.Load ("Prefabs/Item/RedPepper"), transform.position, transform.rotation);
		}
		else if(random <= 65)
		{
			//Damage -20%
			Instantiate (Resources.Load ("Prefabs/Item/Shield"), transform.position, transform.rotation);
		}
		else if(random <= 70)
		{
			//Invicible
			Instantiate (Resources.Load ("Prefabs/Item/Star"), transform.position, transform.rotation);
		}
		else if(random <= 85)
		{
			//Exp 1000
			Instantiate (Resources.Load ("Prefabs/Item/EXP_Stone"), transform.position, transform.rotation);
		}
		else
		{
			//Money 500
			Instantiate (Resources.Load ("Prefabs/Item/Money"), transform.position, transform.rotation);
		}
	}
}
