using UnityEngine;
using System.Collections;

[AddComponentMenu("gui control/Joystick")]
public class Joystick : MonoBehaviour
{
	// assets
	public Texture2D	bgTexture;
	public Rect bgRect; // = new Rect(20,-304,284,284)
	public Vector2 bgCenterOffset; // relative to bgRect.x,y
	public float bgRadius; // = 100
	public float bgDetectRadius; // = 150
	public Texture2D fgTexture;
	public Rect fgRect; // = new Rect(-62,-62,124,124) // x,y is relative from center of fgTexture
	public float fgDisplayRadius; // = 40
	public TextAnchor	rectAnchor; // TextAnchor.LowerLeft
	
	public Color enableColor = Color.white;
	public Color disableColor = Color.grey;
	
	// calculation base on bgRadius, but detection allow up to bgDetectRadius, and fg display bound by fgDisplayRadius
	
	// sub scene object
	public GUITexture bgGUITexture;
	public GUITexture fgGUITexture;
	Vector3	fgStartPosition;
	
	// setting
	public string inputAxisNameX; // = "Horizontal"
	public string inputAxisNameY; // = "Vertical"
	public bool	circleArea = true; // otherwise is rectangle
	public float	deadZoneTouch = 0.2f;
	public bool	fullOutsideDeadZone;
	
	protected Vector2 axisValue; // -1 to 1
	protected Vector2 axisValueAfterDeadZone; // -1 to 1
	const int FINGERID_NONE = -1;
	const int FINGERID_MOUSE = -2;
	const int FINGERID_INPUTAXIS = -3;
	protected int lastFingerId = FINGERID_NONE;
	
	protected bool ignoreInput;
	
	public Color EnableColor
	{
		get { return enableColor; }
		set
		{
			enableColor = value;
			if (enabled)
			{
				if (bgGUITexture != null)
					bgGUITexture.color = enableColor * 0.5f;
				if (fgGUITexture != null)
					fgGUITexture.color = enableColor * 0.5f;
			}
		}
	}
	
	public Color DisableColor
	{
		get { return disableColor; }
		set
		{
			disableColor = value;
			if (!enabled)
			{
				if (bgGUITexture != null)
					bgGUITexture.color = disableColor * 0.5f;
				if (fgGUITexture != null)
					fgGUITexture.color = disableColor * 0.5f;
			}
		}
	}
	
	// Use this for initialization
	void Start()
	{
		useGUILayout = false;
		fgStartPosition = fgGUITexture.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update()
	{
		int newFingerId = FINGERID_NONE;
		Vector2 newAxisValue = Vector2.zero;
		
		bgRadius = Mathf.Max( bgRadius, 1.0f );
		if (!ignoreInput)
		{
			Vector2 scaleVector = GuiButton.getGUIScale();
			Rect scaledDetectRect = GuiButton.transformRect( new Rect(bgRect.x+bgCenterOffset.x-bgDetectRadius, bgRect.y+bgCenterOffset.y-bgDetectRadius, bgDetectRadius+bgDetectRadius, bgDetectRadius+bgDetectRadius), scaleVector, rectAnchor );
			
			if (Input.touchCount > 0)
			{
				// not using getGUIRect, but ScaleRect
				for(int t = 0; t < Input.touchCount; ++t)
				{
					Touch touch = Input.GetTouch(t);
					Vector2 yDownPos = new Vector2( touch.position.x, Screen.height - touch.position.y );
					// assume y down
					if (touch.phase == TouchPhase.Began && scaledDetectRect.Contains(yDownPos)
						|| (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary) && lastFingerId == touch.fingerId )
					{
						if (newFingerId == FINGERID_NONE || touch.fingerId == lastFingerId) // bias more to lastFingerId
						{
							newFingerId = touch.fingerId;
							newAxisValue.x = (yDownPos.x - (scaledDetectRect.x + scaledDetectRect.width * 0.5f)) / (bgRadius * scaleVector.x);
							newAxisValue.y = ((scaledDetectRect.y + scaledDetectRect.height * 0.5f) - yDownPos.y) / (bgRadius * scaleVector.y);

							// magnitude may > 1 or within deadzone, check later
						}
					}
					else if ( (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) && lastFingerId == touch.fingerId)
					{
						newFingerId = FINGERID_NONE;
						break;
					}
				}
			}
			
			if (Input.touchCount == 0 && Input.GetMouseButton(0))
			{
				// not using getGUIRect, but ScaleRect
				// assume Y up/down?
				Vector2 yDownPos = new Vector2( Input.mousePosition.x, Screen.height - Input.mousePosition.y );
				if (Input.GetMouseButtonDown(0) && scaledDetectRect.Contains(yDownPos)
					|| FINGERID_MOUSE == lastFingerId)
				{
					if (newFingerId == FINGERID_NONE || FINGERID_MOUSE == lastFingerId) // bias more to lastFingerId
					{
						newFingerId = FINGERID_MOUSE;
						newAxisValue.x = (yDownPos.x - (scaledDetectRect.x + scaledDetectRect.width * 0.5f)) / (bgRadius * scaleVector.x);
						newAxisValue.y = ((scaledDetectRect.y + scaledDetectRect.height * 0.5f) - yDownPos.y) / (bgRadius * scaleVector.y);
						// magnitude may > 1 or within deadzone, check later


					}
				}
			}
			else if (Input.GetMouseButtonUp(0) && lastFingerId == FINGERID_MOUSE)
			{
				newFingerId = FINGERID_NONE;
			}

			float inputAxisXValue = string.IsNullOrEmpty(inputAxisNameX) ? 0.0f : Input.GetAxis(inputAxisNameX);
			float inputAxisYValue = string.IsNullOrEmpty(inputAxisNameY) ? 0.0f : Input.GetAxis(inputAxisNameY);
			if (inputAxisXValue != 0.0f || inputAxisYValue != 0.0f)
			{
				if (newFingerId == FINGERID_NONE || FINGERID_INPUTAXIS == lastFingerId) // bias more to lastFingerId
				{
					newFingerId = FINGERID_INPUTAXIS;
					newAxisValue.x = inputAxisXValue;
					newAxisValue.y = inputAxisYValue;
				}
			}
			else if (lastFingerId == FINGERID_INPUTAXIS)
			{
				newFingerId = FINGERID_NONE;
			}
		}
			
		lastFingerId = newFingerId; // may be -1 means no touch
		if (lastFingerId != FINGERID_NONE)
		{
			if (circleArea)
			{
				axisValue = Vector2.ClampMagnitude( newAxisValue, 1.0f );
				axisValueAfterDeadZone = axisValue;
				if (lastFingerId != FINGERID_INPUTAXIS)
				{
					if (axisValue.sqrMagnitude <= deadZoneTouch*deadZoneTouch)
						axisValueAfterDeadZone = Vector2.zero;
					else if (fullOutsideDeadZone)
						axisValueAfterDeadZone = axisValue.normalized;
				}
			}
			else
			{
				axisValue.x = Mathf.Clamp(newAxisValue.x, -1.0f, 1.0f );
				axisValue.y = Mathf.Clamp(newAxisValue.y, -1.0f, 1.0f );
				axisValueAfterDeadZone = axisValue;
				if (lastFingerId != FINGERID_INPUTAXIS)
				{
					if (!fullOutsideDeadZone)
					{
						if (Mathf.Abs(axisValue.x) <= deadZoneTouch && Mathf.Abs(axisValue.y) <= deadZoneTouch)
							axisValueAfterDeadZone = Vector2.zero;
					}
					else 
					{
						// will become 8 direction digital pad
						axisValueAfterDeadZone.x = Mathf.Abs(axisValue.x) <= deadZoneTouch ? 0.0f : Mathf.Sign(axisValue.x);
						axisValueAfterDeadZone.y = Mathf.Abs(axisValue.y) <= deadZoneTouch ? 0.0f : Mathf.Sign(axisValue.y);
					}
				}
			}


		}
		else
		{
			axisValue = Vector2.zero; // or move towards zero
			axisValueAfterDeadZone = axisValue;

		}
		
		// move fgGUITexture
		
		if (fgGUITexture != null)
		{
			Vector2 clampFgOffset = Vector2.ClampMagnitude( new Vector2( axisValue.x * bgRadius, axisValue.y * bgRadius ), fgDisplayRadius );
			fgGUITexture.transform.localPosition = fgStartPosition + new Vector3(clampFgOffset.x, clampFgOffset.y);
		}
	}
	
	//~ void OnGUI()
	//~ {
		//~ if (Event.current.type == EventType.Repaint)
		//~ {
			//~ Vector2 scaleVector = getGUIScale();
			//~ GUI.DrawTexture( transformRect( bgRect, scaleVector, rectAnchor ), bgTexture );
			
			//~ Vector2 clampFgOffset = Vector2.ClampMagnitude( new Vector2( axisValue.x * bgRadius, axisValue.y * bgRadius ), fgDisplayRadius );
			
			//~ //bgRect.xy + bgCenterOffset + axisValue * bgRadius + fgRect.xy
			//~ Rect fgUnscaleRect = new Rect( bgRect.x + bgCenterOffset.x + clampFgOffset.x + fgRect.x,
				//~ bgRect.y + bgCenterOffset.y - clampFgOffset.y + fgRect.y,
				//~ fgRect.width, fgRect.height );
			//~ GUI.DrawTexture( transformRect( fgUnscaleRect, scaleVector, rectAnchor ), fgTexture );
		//~ }
	//~ }
	
	public float GetXAxis()
	{
		return axisValueAfterDeadZone.x;
	}
	public float GetYAxis()
	{
		return axisValueAfterDeadZone.y;
	}
	
	public void SetEnable( bool enableFlag )
	{
		enabled = enableFlag;
		
		if (!enableFlag)
		{
			lastFingerId = FINGERID_NONE; // may be -1 means no touch
			axisValue = Vector2.zero;
			axisValueAfterDeadZone = Vector2.zero;
		
			if (fgGUITexture != null)
			{
				Vector2 clampFgOffset = Vector2.ClampMagnitude( new Vector2( axisValue.x * bgRadius, axisValue.y * bgRadius ), fgDisplayRadius );
				fgGUITexture.transform.localPosition = fgStartPosition + new Vector3(clampFgOffset.x, clampFgOffset.y);
			}
		}
		
		if (fgGUITexture != null)
		{
			fgGUITexture.color = (enabled ? enableColor : disableColor) * 0.5f;
		}
		if (bgGUITexture != null)
		{
			bgGUITexture.color = (enabled ? enableColor : disableColor) * 0.5f;
		}
	}
		
	public void SetInputEnable( bool enableInputFlag )
	{
		ignoreInput = !enableInputFlag;
	}
	
	public bool GetInputEnable()
	{
		return !ignoreInput;
	}
	

}
