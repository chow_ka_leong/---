﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("gui control/BattleGUI")]
public class BattleGUI : MonoBehaviour {

	public string element;

	GuiButton pauseBtn;

	GuiButton resumeBtn;
	GuiButton quitBtn;

	GameObject pause_menu;

	bool isPause = false;

	// Use this for initialization
	void Start () {

		GameObject pauseobj = GameObject.Find ("pauseButton") as GameObject;
		pauseBtn = pauseobj.GetComponent ("GuiButton") as GuiButton;

		GameObject resumeobj = GameObject.Find ("continueButton") as GameObject;
		resumeBtn = resumeobj.GetComponent ("GuiButton") as GuiButton;

		GameObject quitobj = GameObject.Find ("quitButton") as GameObject;
		quitBtn = quitobj.GetComponent ("GuiButton") as GuiButton;

		pause_menu = GameObject.Find ("PauseMenu") as GameObject;


		switch(element)
		{
			case "attack_1":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.9f,Screen.height*0.15f,0,0);
			break;

			case "dodge":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.78f,Screen.height*0.08f,0,0);
			break;

			case "skill":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.8f,Screen.height*0.3f,0,0);
			break;

			case "change":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.92f,Screen.height*0.38f,0,0);
			break;

			case "char_icon":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.1f,Screen.height*0.9f,0,0);
			break;

			case "hp_bg":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.295f,Screen.height*0.93f,0,0);
			break;

			case "hp":
			//guiTexture.pixelInset = new Rect(183,Screen.height*0.932f,0,0);
			break;

			case "timer_title":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.55f,Screen.height*0.932f,0,0);
			break;

			case "time_1":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.65f,Screen.height*0.932f,0,0);
			break;

			case "time_2":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.67f,Screen.height*0.932f,0,0);
			break;

			case "sep_1":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.69f,Screen.height*0.932f,0,0);
			break;

			case "time_3":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.71f,Screen.height*0.932f,0,0);
			break;

			case "time_4":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.73f,Screen.height*0.932f,0,0);
			break;

			case "sep_2":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.75f,Screen.height*0.932f,0,0);
			break;

			case "time_5":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.77f,Screen.height*0.932f,0,0);
			break;

			case "time_6":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.79f,Screen.height*0.932f,0,0);
			break;

			case "pause":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.95f,Screen.height*0.932f,0,0);
			break;

			case "pause_bg":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.5f,Screen.height*0.5f,0,0);
			break;

			case "continueButton":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.5f,Screen.height*0.6f,0,0);
			break;

			case "quitButton":
			GetComponent<GUITexture>().pixelInset = new Rect(Screen.width*0.5f,Screen.height*0.4f,0,0);
			break;
			
		}
	}

	void Update () {
		

		if (pauseBtn.GetButtonDown ()) {
			Time.timeScale = 0;

			pause_menu.transform.localScale = new Vector3(1,1,1);
			isPause = true;
		}

		if(!isPause)
			return;

		if(resumeBtn){
			if (resumeBtn.GetButtonDown ()) {
				Time.timeScale = 1;
				pause_menu.transform.localScale = new Vector3(0,0,0);
				isPause = false;
			}
		}

		if(quitBtn){
			if (quitBtn.GetButtonDown ()) {
				Time.timeScale = 1;
				Application.LoadLevel("MainUI");
			}
		}

		
	}

}
