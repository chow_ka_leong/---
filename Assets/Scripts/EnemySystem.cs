﻿using UnityEngine;
using System.Collections;

public class EnemySystem : MonoBehaviour {

	public Transform[] spawnPoint;

	public int total_wave;

	public string[] wave;

	public GameObject[] enemies;

	public GameManager manager;

	int wave_index = 1;

	bool gameover = false;

	float interval = 3.0f;

	// Use this for initialization
	void Start () {
		//InvokeRepeating ("Spawn", interval, interval);

		generateWave ();

		//manager.showEarning ();

	}

	void Update(){

		if(gameover){

			return;
		}
			

		if (GameObject.FindWithTag ("Enemy") == null) {
		
			wave_index++;

			if(wave_index <= total_wave)
			{
				if(wave_index == total_wave)
					Instantiate (Resources.Load ("Prefabs/Warning"),new Vector3 (1.15f, 0.56f, -5.57f), Quaternion.identity);
				generateWave ();
			}
			else
			{
				print("Clear");
				gameover = true;
				//Level Clear
				StartCoroutine(showEarning(3.0f));
			}


		}
	
	}

	void Spawn(string order)
	{
		string[] split = order.Split (":" [0]);

		int enemy_index = int.Parse (split [0]);
		int sum_of_enemy = int.Parse (split [1]);



		for(int i = 0; i < sum_of_enemy; i++)
		{
			Transform point = spawnPoint[Random.Range (0, spawnPoint.Length)];

			Instantiate (enemies[enemy_index-1], point.position, point.rotation);
		}
		//Instantiate (enemy01, spawnPoint.position, spawnPoint.rotation);

	}

	void generateWave()
	{

		string info = wave [wave_index - 1];

		string[] _info = info.Split (","[0]);

		foreach(string str in _info)
		{
			Spawn(str);
		}
	}

	IEnumerator showEarning(float waitingTime)
	{
		yield return new WaitForSeconds(waitingTime);
		manager.showEarning ();
	}

	IEnumerator back(float waitingTime)
	{
		yield return new WaitForSeconds(waitingTime);
		Application.LoadLevel("MainUI");
	}
}
