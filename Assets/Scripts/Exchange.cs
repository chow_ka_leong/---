﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Exchange : MonoBehaviour {

	//1 or 0
	public int mode = 0;

	public Image money_unit;
	public Image diamond_unit;

	// Use this for initialization
	void Update () {
		if(mode == 0){
			money_unit.sprite = Resources.Load <Sprite>("MainUI/UI_兌換金幣/UI/number[size_48.49]/1");
			diamond_unit.sprite = Resources.Load <Sprite>("MainUI/UI_兌換金幣/UI/number[size_48.49]/3");
		}
		else{
			money_unit.sprite = Resources.Load <Sprite>("MainUI/UI_兌換金幣/UI/number[size_48.49]/3");
			diamond_unit.sprite = Resources.Load <Sprite>("MainUI/UI_兌換金幣/UI/number[size_48.49]/9");
		}
			
	}

	public void close(){

		gameObject.SetActive (false);

	}

	public void buy(){
		
		if(mode == 0){
			if(PlayerPrefs.GetInt ("diamond") >= 3){
				
				int d = PlayerPrefs.GetInt ("diamond");
				PlayerPrefs.SetInt("diamond",d-3);
				PlayerPrefs.Save();

				int m = PlayerPrefs.GetInt ("money");
				PlayerPrefs.SetInt("money",m+1000);
				PlayerPrefs.Save();
				
			}
		}
		else{
			if(PlayerPrefs.GetInt ("diamond") >= 9){

				int d = PlayerPrefs.GetInt ("diamond");
				PlayerPrefs.SetInt("diamond",d-9);
				PlayerPrefs.Save();
				
				int m = PlayerPrefs.GetInt ("money");
				PlayerPrefs.SetInt("money",m+3000);
				PlayerPrefs.Save();
				
			}
		}
		
	}
	

}
