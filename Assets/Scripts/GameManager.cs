﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public PlayerManager manager;
	public Global global;
	public CameraMovement camera;

	//Mission Completed
	public GameObject missionCompletePanel;
	public Image char01;
	public Image char02;

	public Image exp1;
	public Image exp2;

	public Text char1_remain;
	public Text char2_remain;
	public Text money_reward;

	bool p1_levelup = false;
	bool p2_levelup = false;

	//levelup Panel
	private int p1_original_level;
	private int p2_original_level;
	public GameObject levelupPanel;
	public Image char_tn;
	public Text old_lv;
	public Text new_lv;

	public Text old_hp;
	public Text new_hp;

	public Text old_atk;
	public Text new_atk;

	private GameObject Dialogue;

	void Awake () {

		Time.timeScale = 0;

		string p1 = global.getPlayerID (0);
		string p2 = global.getPlayerID (1);

		GameObject player1 = null;
		GameObject player2 = null;

		if(p2 != ""){
			player2 = (GameObject)Instantiate (Resources.Load ("Prefabs/Character/SKY0" + p2), Vector3.zero, Quaternion.identity);
			PlayerMovement move2 = player2.GetComponent<PlayerMovement> ();
			move2.HP = calHP(p2);
			move2.ATK = calATK (p2);
			
			player2.SetActive (false);
		}

		player1 = (GameObject)Instantiate (Resources.Load ("Prefabs/Character/SKY0" + p1), Vector3.zero, Quaternion.identity);

		//Load Character Stat
		PlayerMovement move1 = player1.GetComponent<PlayerMovement> ();
		move1.HP = calHP(p1);
		move1.ATK = calATK (p1);

		manager.setPlayers (player1, player2);
		camera.setWatchTarget (player1.transform);

		char01.sprite = Resources.Load<Sprite> ("TeamUI/CHAR_ICON/CHAR_ICON_SKY0" + p1);
		char02.sprite = Resources.Load<Sprite> ("TeamUI/CHAR_ICON/CHAR_ICON_SKY0" + p2);

		Dialogue = GameObject.Find ("Dialogue");


	}

	float calHP(string ID)
	{
		Dictionary<string, string> dict = CharacterStat.getCharacterData (ID);

		string[] character = PlayerPrefsX.GetStringArray("Char"+ID);

		float hp = float.Parse (dict ["hp"]);
		int level = int.Parse (character [0]);
		float final = (float.Parse (CharacterUpgrade.getCharData (level)["hp"]) + (hp - 1000.0f)/2) * (1 + 0.05f * float.Parse (dict ["rare"]) + float.Parse(TypeUpgrade.getCharData (dict ["upgrade_type"])["hp"]));

		if(level > 1)
			return final;
		else
			return hp;
	}

	float calHP(string ID, string _level)
	{
		Dictionary<string, string> dict = CharacterStat.getCharacterData (ID);
		
		string[] character = PlayerPrefsX.GetStringArray("Char"+ID);
		
		float hp = float.Parse (dict ["hp"]);
		int level = int.Parse (_level);
		float final = (float.Parse (CharacterUpgrade.getCharData (level)["hp"]) + (hp - 1000.0f)/2) * (1 + 0.05f * float.Parse (dict ["rare"]) + float.Parse(TypeUpgrade.getCharData (dict ["upgrade_type"])["hp"]));
		
		if(level > 1)
			return final;
		else
			return hp;
	}

	float calATK(string ID)
	{
		Dictionary<string, string> dict = CharacterStat.getCharacterData (ID);
		
		string[] character = PlayerPrefsX.GetStringArray("Char"+ID);
		
		float atk = float.Parse (dict ["atk"]);
		int level = int.Parse (character [0]);
		float final = (float.Parse (CharacterUpgrade.getCharData (level)["atk"]) + (atk - 300.0f)/2) * (1 + 0.05f * float.Parse (dict ["rare"]) + float.Parse(TypeUpgrade.getCharData (dict ["upgrade_type"])["atk"]));
		
		if(level > 1)
			return final;
		else
			return atk;
	}

	float calATK(string ID, string _level)
	{
		Dictionary<string, string> dict = CharacterStat.getCharacterData (ID);
		
		string[] character = PlayerPrefsX.GetStringArray("Char"+ID);
		
		float atk = float.Parse (dict ["atk"]);
		int level = int.Parse (_level);

		float final = (float.Parse (CharacterUpgrade.getCharData (level)["atk"]) + (atk - 300.0f)/2) * (1 + 0.05f * float.Parse (dict ["rare"]) + float.Parse(TypeUpgrade.getCharData (dict ["upgrade_type"])["atk"]));
		
		if(level > 1)
			return final;
		else
			return atk;
	}

	public void showEarning()
	{
		string stage = PlayerPrefs.GetString ("Stage");
		
		int c = int.Parse(stage.Substring (0, 1));
		int l = int.Parse(stage.Substring (2, 1));

		int offset_now = (Global.chapter - 1) * 5 + Global.level;
		int offset_prefs = (c - 1) * 5 + l;

		if(offset_now > offset_prefs)
			PlayerPrefs.SetString ("Stage", Global.chapter.ToString() +"-"+Global.level.ToString());

		GameObject gui = GameObject.Find("UI");
		gui.SetActive (false);

		GameObject joy = GameObject.Find("joystick");
		joy.SetActive (false);

		missionCompletePanel.SetActive (true);

		money_reward.text = MoneyReward.getMissionData (Global.getLevel ());
		int _money = PlayerPrefs.GetInt ("money");
		_money += int.Parse(money_reward.text) + manager.extraMoney;
		PlayerPrefs.SetInt ("money", _money);

		//P1
		string p1_level = PlayerPrefsX.GetStringArray ("Char" + global.getPlayerID (0)) [0]; 
		p1_original_level = int.Parse (p1_level);
		int p1_exp_remain = int.Parse (PlayerPrefsX.GetStringArray ("Char" + global.getPlayerID (0)) [1]);
		int p1_total_exp = p1_exp_remain + EXPRewards.getMissionData(Global.getLevel()) + manager.extraEXP;

		if(CheckLevelUp(p1_total_exp, int.Parse(p1_level)))
		{
			p1_levelup = true;
			p1_total_exp -= EXPChart.getEXPData (int.Parse (p1_level));
			p1_level = (int.Parse(p1_level) + 1).ToString();

			while(CheckLevelUp(p1_total_exp, int.Parse(p1_level)))
			{
				p1_total_exp -= EXPChart.getEXPData (int.Parse (p1_level));
				p1_level = (int.Parse(p1_level) + 1).ToString();
			}

			PlayerPrefsX.SetStringArray("Char" + global.getPlayerID (0), new string[]{p1_level, p1_total_exp.ToString()});

		}
		else
		{
			
			PlayerPrefsX.SetStringArray("Char" + global.getPlayerID (0), new string[]{p1_level, p1_total_exp.ToString()});
		}

		float p1_exp_bar_width = (float)p1_total_exp / (float)EXPChart.getEXPData (int.Parse(p1_level));
		exp1.transform.localScale = new Vector2(p1_exp_bar_width, 1);
		char1_remain.text = p1_total_exp.ToString ();


		if(global.getPlayerID (1) != ""){
			//P2
			string p2_level = PlayerPrefsX.GetStringArray ("Char" + global.getPlayerID (1)) [0]; 
			p2_original_level = int.Parse (p2_level);
			int p2_exp_remain = int.Parse (PlayerPrefsX.GetStringArray ("Char" + global.getPlayerID (1)) [1]);
			int p2_total_exp = p2_exp_remain + EXPRewards.getMissionData(Global.getLevel()) + manager.extraEXP;
			
			if(CheckLevelUp(p2_total_exp, int.Parse(p2_level)))
			{
				p2_levelup = true;
				p2_total_exp -= EXPChart.getEXPData (int.Parse (p2_level));
				p2_level = (int.Parse(p2_level) + 1).ToString();
				
				while(CheckLevelUp(p2_total_exp, int.Parse(p2_level)))
				{
					p2_total_exp -= EXPChart.getEXPData (int.Parse (p2_level));
					p2_level = (int.Parse(p2_level) + 1).ToString();
				}
				
				PlayerPrefsX.SetStringArray("Char" + global.getPlayerID (1), new string[]{p2_level, p2_total_exp.ToString()});
				
			}
			else
			{
				
				PlayerPrefsX.SetStringArray("Char" + global.getPlayerID (0), new string[]{p2_level, p2_total_exp.ToString()});
			}
			
			
			
			float p2_exp_bar_width = (float)p2_total_exp / (float)EXPChart.getEXPData (int.Parse(p2_level));
			exp2.transform.localScale = new Vector2(p2_exp_bar_width, 1);
			char2_remain.text = p2_total_exp.ToString ();
		}
		else{
			char02.enabled = false;
			exp2.transform.localScale = new Vector2(0, 1);
		}



		PlayerPrefs.Save ();
	}

	public void complete_Next()
	{
		if(p1_levelup)
		{
			missionCompletePanel.SetActive(false);

			levelupPanel.SetActive(true);

			char_tn.sprite = Resources.Load<Sprite> ("TeamUI/CHAR_ICON/CHAR_ICON_SKY0" + global.getPlayerID (0));

			old_lv.text = p1_original_level.ToString();
			
			old_atk.text = Mathf.RoundToInt( calATK(global.getPlayerID (0), old_lv.text)).ToString();
			
			old_hp.text = Mathf.RoundToInt( calHP(global.getPlayerID (0), old_lv.text)).ToString();

			new_lv.text = PlayerPrefsX.GetStringArray ("Char" + global.getPlayerID (0)) [0];

			new_atk.text = Mathf.RoundToInt( calATK(global.getPlayerID (0), new_lv.text)).ToString();

			new_hp.text = Mathf.RoundToInt( calHP(global.getPlayerID (0), new_lv.text)).ToString();

			p1_levelup = false;
		}
		else if(p2_levelup)
		{
			missionCompletePanel.SetActive(false);
			
			levelupPanel.SetActive(true);
			
			char_tn.sprite = Resources.Load<Sprite> ("TeamUI/CHAR_ICON/CHAR_ICON_SKY0" + global.getPlayerID (1));
			
			old_lv.text = p2_original_level.ToString();
			
			old_atk.text = Mathf.RoundToInt( calATK(global.getPlayerID (1), old_lv.text)).ToString();
			
			old_hp.text = Mathf.RoundToInt( calHP(global.getPlayerID (1), old_lv.text)).ToString();
			
			new_lv.text = PlayerPrefsX.GetStringArray ("Char" + global.getPlayerID (1)) [0];
			
			new_atk.text = Mathf.RoundToInt( calATK(global.getPlayerID (1), new_lv.text)).ToString();
			
			new_hp.text = Mathf.RoundToInt( calHP(global.getPlayerID (1), new_lv.text)).ToString();
			
			p2_levelup = false;
		}
		else
		{
			Story story = Dialogue.GetComponent<Story> ();
			story.End ();
			//Application.LoadLevel("MainUI");
		}
	}

	public void levelup_Next()
	{
		if(p2_levelup)
		{
			missionCompletePanel.SetActive(false);
			
			levelupPanel.SetActive(true);
			
			char_tn.sprite = Resources.Load<Sprite> ("TeamUI/CHAR_ICON/CHAR_ICON_SKY0" + global.getPlayerID (1));
			
			old_lv.text = (int.Parse(PlayerPrefsX.GetStringArray ("Char" + global.getPlayerID (1)) [0]) - 1).ToString();
			
			old_atk.text = Mathf.RoundToInt( calATK(global.getPlayerID (1), old_lv.text)).ToString();
			
			old_hp.text = Mathf.RoundToInt( calHP(global.getPlayerID (1), old_lv.text)).ToString();
			
			new_lv.text = PlayerPrefsX.GetStringArray ("Char" + global.getPlayerID (1)) [0];
			
			new_atk.text = Mathf.RoundToInt( calATK(global.getPlayerID (1), new_lv.text)).ToString();
			
			new_hp.text = Mathf.RoundToInt( calHP(global.getPlayerID (1), new_lv.text)).ToString();
			
			p2_levelup = false;
		}
		else
		{
			Story story = Dialogue.GetComponent<Story> ();
			story.End ();
			//Application.LoadLevel("MainUI");
		}
	}

	bool CheckLevelUp(int exp, int lv)
	{
		if(lv > 29)
			return false;

		if(exp > EXPChart.getEXPData (lv))
			return true;
		else
			return false;
	}
}
