﻿using UnityEngine;
using System.Collections;

public class Arrow_ENE : MonoBehaviour {

	public float atk = 0;

	// Use this for initialization
	void Start () {
		StartCoroutine (CleanUp (10.0f));
	}
	


	IEnumerator CleanUp(float waitTime){
		yield return new WaitForSeconds(waitTime);
		Destroy (this.gameObject);
	}

	public void setATK(float _a)
	{
		atk = _a;
	}

	public float getATK()
	{
		return atk;
	}
}
