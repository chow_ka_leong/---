// Unlit alpha-blended shader.
// - no lighting
// - no lightmap support
// - no per-material color
// texture tiling and offset of alpha texture is using main texture's tiling and offset

Shader "Unlit/Transparent alpha-texture" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_AlphaTex ("Alpha (RGB)", 2D) = "white" {}
}


SubShader {
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	LOD 100
	
	Lighting Off ZWrite Off
	Blend SrcAlpha OneMinusSrcAlpha 

	Pass {
	
		CGPROGRAM

		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"

		sampler _MainTex;
		sampler _AlphaTex;
		float4 _MainTex_ST;
		// float4 _AlphaTex_ST;

		struct v2f {
			float4 vertex : SV_POSITION;
			float2 texcoord : TEXCOORD0;
		};		
		
		v2f vert(appdata_base v) : POSITION
		{
			v2f o;
			o.vertex = mul (UNITY_MATRIX_MVP, v.vertex);
			o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
			return o;
		}
		
		fixed4 frag(v2f i) : COLOR
		{
			fixed4 o;
			o.rgb = tex2D(_MainTex, i.texcoord);
			o.a = tex2D(_AlphaTex, i.texcoord).r;
			return o;
		}

		ENDCG
	}
}

Fallback "Unlit/Transparent"

}