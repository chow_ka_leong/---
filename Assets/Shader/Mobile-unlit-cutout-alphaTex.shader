// Unlit alpha-blended cutout shader.
// - no lighting
// - no lightmap support
// - no per-material color
// alpha-test
// texture tiling and offset of alpha texture is using main texture's tiling and offset

Shader "Mobile/Unlit Cutout alpha-texture" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_AlphaTex ("Alpha (RGB)", 2D) = "white" {}
}


SubShader {
	Tags { "Queue"="AlphaTest-3" "IgnoreProjector"="True" "RenderType"="TransparentCutout" }
	LOD 100
	
	Lighting Off AlphaTest GEqual 0.875
	Blend One Zero

	Pass {
	
		CGPROGRAM

		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"

		sampler _MainTex;
		sampler _AlphaTex;
		float4 _MainTex_ST;
		// float4 _AlphaTex_ST;

		struct v2f {
			float4 vertex : SV_POSITION;
			float2 texcoord : TEXCOORD0;
		};		
		
		v2f vert(appdata_base v) : POSITION
		{
			v2f o;
			o.vertex = mul (UNITY_MATRIX_MVP, v.vertex);
			o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
			return o;
		}
		
		fixed4 frag(v2f i) : COLOR
		{
			fixed4 o;
			if (tex2D(_AlphaTex, i.texcoord).r < 0.875f)
				discard;
			o = tex2D(_MainTex, i.texcoord);
			return o;
		}

		ENDCG
	}
}

Fallback "Unlit/Transparent"

}